.. _u1.03.01:

.. role:: red
.. role:: blue
.. role:: green
.. role:: python(code)
   :language: python

*************************************************
**[U1.03.01]** : Supervisor and command language
*************************************************

The ``Code_Aster`` supervisor controls the execution of commands specified by the user in the
command file.   The role of the supervisor, the command syntax, the use of Python in command
files, and exceptions and their handling are discussed in this document.

1 Introduction 
==============

The supervisor controls the execution and administration of commands during the course of execution
of a ``Code-Aster`` simulation.  The commands are largely provided by the user.  The formalization 
of communications between the code and its user is referred to as the "command language".

Python is used to create a command dictionary, control the supervisor, and in the user command
files.  That makes it possible to free the supervisor from the task of syntactic analysis, which
is reserved for Python itself.

A command file contains a series of Python function calls (commands) which are defined in the 
internal ``Aster`` command dictionary.
These functions have entry arguments (keywords and their contents) and exit arguments (concepts
created by the command).  The author of the command file is required to adhere to Python syntax
and with rules imposed by the command dictionary (arguments are coherent with the command dictionary).


2 How the supervisor works
==========================

.. note::

  Section 2 can be skipped by a new user of the code.

2.1 General architecture
------------------------

.. index:: Dictionary, Catalog, Supervisor, JDC, Step

The basic elements that are involved in the execution of an ``Aster`` calculation are:

* the command file, provided by the user
* the command :blue:`dictionary` (also called :blue:`Catalog`): this is a Python module called ``cata`` 
  that can be found in the ``Cata`` package
* the high level :blue:`Supervisor` object
* the :blue:`JDC` object created by the supervisor which is finally executed.

The :blue:`Supervisor` object is a Python object which analyzes the options transmitted on the command
line, imports the command dictionary, creates the :blue:`JDC` object and executes it.

The :blue:`JDC` object (a short name for the set of commands) is a Python object created by the 
:blue:`Supervisor` object starting from the text of the command file and the commands stored
in the command dictionary module.   It contains :blue:`Step` objects.  The :blue:`JDC` object is
an internal representation of the user-generated command file.

:blue:`Step` objects represent each call ``Aster`` commands in the command file. Each :blue:`Step`
object is named on the basis of the command that it references, the list of the active keywords and 
their values, and the type and name of the product (output) concept.

The construction, followed by the execution, of a :blue:`JDC` object involves the following actions:

* **syntactic analysis of the user command file**: it is at this stage that Python syntax is checked
  (brackets, commas between keywords, indentation, etc.).   If an error is detected 
  (Python ``SyntaxError``), the execution of ``Aster`` is halted.  This type of error is fatal, and
  further processing is discontinued.
* **construction of the steps**: that consists in creating a :blue:`Step` object for each call to 
  an ``Aster`` command in the command file.  This object is registered by :blue:`JDC` which manages the
  list of steps and the associated concepts. 
* **verification of each step**: if the call to a command in the user file is incompatible with 
  the command dictionary, a report is displayed and the execution is stopped.  This is the
  "semantic verification" step.
*  **execution of the commands**: for each step in the command, the action calls the appropriate
   high level FORTRAN routine (``op0nnn.f``).


2.2 Global execution (or step-by-step)
--------------------------------------

A command set can be built and executed in two modes:

* the :blue:`global` mode, in which **all** the steps of the command set are built then executed in
  their order of appearance. This mode is activated by the keyword :python:`PAR_LOT='OUI'` in the
  command :python:`DEBUT()`.

* the :blue:`step-by-step` mode, in which each step is executed immediately after its construction.
  This mode is activated by the keyword :python:`PAR_LOT='NON'` in the :python:`DEBUT()` command.

The :blue:`global` mode is activated by default (:python:`DEBUT()` command with no arguments).  Each 
mode has its advantages and disadvantages.
 
The :blue:`global` procedure guarantees that all the input files are semantically correct before starting
a calculation which could fail or fail to converge.  This model prevents fatal errors due to
mistakes in the input file, e.g., forgotten keywords,  after a long computation.  There is also
the advantage that all the stages of the command set are built and stored in this mode. If several 
thousands of steps are involved, the global model can consume a lot of memory and is not advised.

The :blue:`step-by-step` mode builds a step only after having executed the preceding one. It thus detects
only the semantic errors of the next pending command of a sequence of commands.  If a step takes a lot
of time, the entire process has to be repeated in case of a fatal error in a later command.   However, it
makes it possible to exploit a computed result (in a concept) in the command file by placing conditional
instructions there.  In this mode, the completed step is released at once from memory. 

**Example:**

Here is an example of a loop with a stop criterion on the value of a calculated quantity, stored in
a concept of type ``table`` (:python:`stress[k]`). If a mandatory keyword is missing in the call to
:python:`POST_RELEVE_T`, that will be detected only after the complete execution of the first 
:python:`MECA_STATIQUE`.  On the other hand, the :blue:`step-by-step` mode makes possible to assign 
a value to the variable :python:`stress_eq` since the concept :python:`stress[k]` has been completely 
calculated by the time the supervisor executes this line.

::

   DEBUT(PAR_LOT='NON')

   result = [None] * 10
   stress = [None] * 10

   for k in range(1, 10):

      result[k] = MECA_STATIQUE(...)
      stress[k] = POST_RELEVE_T(...)
      stress_eq = stress[k][‘VMIS’, 4]
     
      if stress_eq < criterion:
        break

   FIN()

.. note::

  The choice execution model determines the order in which the analysis **semantics** will proceed
  (:blue:`step-by-step` or :blue:`global` for the whole :blue:`JDC`). But, in both cases, the 
  **syntactic** analysis of Python is always carried out first for the entire command file.

.. important::

  ``EFICAS`` can only generate and read command sets containing **exclusively** ``Aster`` commands, 
  without other Python instructions; this regardless of the mode (global or step-by-step) chosen.

2.3 The construction of the steps
----------------------------------

During the construction of each :blue:`Step` object, its semantic coherence is checked using the
command dictionary.  Any error that is detected is written to a report which, in the
global mode, is output after the analysis of the entire command file.

Examples of semantic checks:

* respect for the number of keyword arguments
* respect for the type of argument
* membership of an argument in a list of possible values
* accuracy of the spelling of a keyword or a keyword argument
* compliance with the rules of exclusion or other relationships between keywords
* presence of mandatory keywords

At this stage, if the command is an operator and produces a concept, the return value is
assigned the appropriate type. The :blue:`Supervisor` checks whether a concept with the same name 
has already been defined, or if it is being ``reused``, and that the command authorizes it.

2.4 Macro-commands 
------------------

.. index:: macro-command, MACRO_ETAPE, DEBUT, FORMULE, INCLUDE, INCLUDE_MATERIAU, POURSUITE

A macro-command, from the point of view of the user, is an ordinary order.  A macro, does not call a 
high level FORTRAN routine directly but generates other commands.

Two types of macro-commands exist:

* Python macros
* supervisor macros: these are the special commands (:python:`DEBUT()`,  :python:`FORMULE()`,  
  :python:`INCLUDE()`, :python:`INCLUDE_MATERIAU()`, :python:`POURSUITE()`) which have to be
  processed during construction.

Similar to :blue:`JDC`, the call to a macro-command produces a parent object (of type  
:python:`MACRO-ETAPE`) which contains child objects such as the steps which the macro generates, 
and even other macros.

An macro-command of the :blue:`JDC` is first treated like the other commands (with syntactic checking, 
construction of the macro step). Then it is "built" by applying the Python method :python:`Build`
on the :blue:`JDC` object. After its construction, the command steps produced by the macro are 
substituted for the step of the macro itself, for later execution.

.. important::

  The construction of the macro-commands step proceeds right before their execution, and not at the 
  time the global pass on the command file in :python:`PAR_LOT='OUI'` mode.  That has two consequences:

  * `EFICAS` analyzes the syntax of the macro-command itself, but not that of its child commands.
  * On the other hand, you can use, in the programming of macros, data previously calculated and 
    retrieved in the python namespace, without having to impose the mode :python:`PAR_LOT = 'NON'` 
    to the user of the macro.

2.5 Start-up procedures
-----------------------

.. index:: DEBUT, POURSUITE, PAR_LOT, Supervisor, Jeveux

The available start-up procedures are: :python:`DEBUT` (:ref:`[U4.11.01] <u4.11.01>`) and 
:python:`POURSUITE` (**[U4.11.03]**).

One of these two procedures **must** be present in the command file. No other 
``Aster`` command must precede them. If that is the case or if neither is present, the execution 
will be stopped as soon as the :blue:`JDC` is created.  The procedures contain information 
(:python:`PAR_LOT='OUI' or 'NON'`) which determine the mode of the commands that follow.

These :blue:`Supervisor` macro commands call FORTRAN routines for
 * connection of logical units to the files
 * opening of databases
 * reading the catalog of elements.

The first task is to match logical unit numbers of files: standard input / output (message, error, result).

The second task is to define and open the databases (direct access file used by memory manager) 
according to user instructions. The user can redefine parameters of these files (see documents 
:ref:`[U4.11.01] <u4.11.01>` and **[U4.11.03]** on the start-up procedures). For that the initialization routines
of `Jeveux` are called (see document **[D6.02.01]** The Memory manager, Jeveux).

The sequence of commands to be executed necessarily ends with the :python:`FIN()` command. Any text
that follows :python:`FIN()` must be commented out (i.e. start with `#`). For an included file, this is 
the :python:`return` command which marks the end of the instructions that ``Aster`` must take into account.

.. note::

   In interactive mode, enter commands by hand, do not enter a :python:`FIN()` command, and pass
   the ``–interact`` argument on the job submission command line.

2.6 Links with EFICAS
---------------------

.. index:: EFICAS

The kernel of the supervisor is common with :blue:`EFICAS`, the ``Aster`` command file editor. During
the editing of a command file, this tool carries out the syntactic analysis and the checks the coherence 
of the concepts by constructing the :blue:`JDC` and its :blue:`Step` objects. :blue:`EFICAS` does 
not carry out the task of executing commands because that would require the ``Aster`` source code.

3 The process control language
==============================

3.1 Python and the language process control
-------------------------------------------

.. index:: Python

A command file for ``Code_Aster`` is exclusively made up of Python instructions.  The file, therefore, 
has to conform to the rules of this language.  For information on Python, you can read Python tutorials
(www.python.org) or introductory books on Python.  However, a deep knowledge of Python is not necessary for
the use of ``Aster``.

A command file can contain Python instructions two types: ``Aster`` commands, and any other Python
instruction. Indeed, a command file is a complete Python program.  We can place 
control structures (loops), tests (if), numerical calculations, calls to pre- and post-processing 
functions etc., in the command file.

Within the framework of a ``classical`` use of the code where the command file contains 
only ``Aster`` commands, the two rules specific to Python that are retained are:

* No indentation on the first declaration line of an instruction::

    mesh = LIRE_MAILLAGE()

  Neither blank nor tab should be placed before the ``mesh`` string.

* Arguments of functions (keywords of commands) are separated by commas; 
  they are composed of a keyword, sign “=”, and then the contents of the keyword.

.. image:: u4.01.00/basic_syntax.svg
   :height: 200px
   :align:  center

.. important::

   The ``EFICAS`` editor can produce only exclusively ``Aster`` commands.
   The use of ``EFICAS`` guarantees three things:

   * the generated file will have a correct Python syntax
   * the generated commands will be coherent with the command dictionary,
   * the generated concepts will be correctly connected (e.eg., no use of a concept without it 
     having been created by a preceding order).

   The user can then be assured that execution will not stop due to a syntax problem.


3.2 Aster ``concept``
---------------------

.. index: ! concept
.. index: TABLE

.. admonition:: Definition

   An ``Aster`` :blue:`concept` is a data structure that the user can handle and name.  These
   concepts assigned data-types at the time of their creation and can be used only as an input 
   argument of same the in a subsequent command.

The notion of concept therefore allows the user to manipulate objects symbolically and regardless 
of their internal representation (which he may not know). Moreover, the Python object designated 
by the name of the concept does not contain any information other than its type, its class in the 
Python sense (cf. **doc D**).  Its name, transmitted by the :blue:`Supervisor` to FORTRAN, allows 
``Aster`` to find the corresponding data structure in the global database. But it is not possible 
to examine the data structure from the command file. For example, you cannot examine the mesh
data structure, named ``mesh`` and of type ``maillage``, using the following Python code ::   

  mesh = LIRE_MAILLAGE()
  print(mesh)

This command will generate the message::

  <Cata.cata.maillage_sdaster object At 0x593cad0>

There is an exception to this rule: :blue:`tables`.  It is possible  to recover information 
contained in a data structure of type :blue:`TABLE` by handling it like a two-dimensional array:

For example, to print a value::

  print(result['DX', 1] 

or to assign it to a variable::

  value = result['DX', 1]

That supposes, of course, that the data structure ``result``, of type :blue:`TABLE`, has already 
been calculated at the time when this instruction is encountered, i.e., in :blue:`step-by-step`
mode (:python:`PAR_LOT='NON'`).

.. admonition:: Lexical note

  Concept names must not exceed 8 characters. Alphanumeric characters are allowed (lower- and upper-case 
  letters and numbers not placed in the first position) as well as the underscore '_'. Case is important: 
  the concepts "MESH" and "Mesh" can be used in the same command file and will be considered as 
  different. This type of naming is, however, not recommended for the readability of the file.

3.3 Possible operations
-----------------------

.. index:: procedure, operator

The structure of the command language is in the form of a linear sequence of instructions. Besides 
the Python instructions other than ``Aster`` commands, three types of instructions are available:

* an operator that performs an action and provides a product concept of a predefined type
  can be used by the subsequent instructions in the command set
* a procedure which performs an action but does not generate a concept
* the macro-command which generates a sequence of instructions of the two preceding types and
  which can produce zero, one, or more concepts.

Typically, an operator will be an assignment or solution command, a procedure will be a
print command (to a file).

The general syntax of an operator has the form::

  concept_name = operator(arguments ...) 

whereas a procedure has the form::

  procedure(arguments ..

More detail on the syntax of an operator or a procedure is given below.

3.4 Rules for product concepts operators
-----------------------------------------

3.4.1 Basic principle
^^^^^^^^^^^^^^^^^^^^^

.. index:: product concept

With each execution of an operator, a new concept is produced (of the predefined type in
the command dictionary) called the :blue:`product concept`.

Concepts appearing in input arguments of commands are not modified. 

3.4.2 Product concept and reused concept
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index:: reused concept
.. index:: renentrant, restart

A :blue:`reused concept` is one that, after being produced by an operator, is modified by a 
new instance of this operator or by another operator.  The use of a reused concept is only possible, 
as a departure from the :blue:`Basic Principle`, under only two conditions

* the command dictionary allows the use of reusable concepts for the operator: the ``reentrant`` 
  (:green:`restart`)
  attribute of the dictionary command is 'o' or 'f'

* the user explicitly requests the reuse of a product concept with the attribute
  :python:`reuse = concept_name` in the arguments of the commands which allow it.

3.4.3 Checks carried out by the supervisor on product concepts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* For :blue:`product concept` that follows the default rule, 
  the :blue:`Supervisor` checks that the name of the product concept has not already been
  allotted by one of the preceding commands, in particular by a command from a previous in the case of
  a :python:`POURSUITE` or an :python:`INCLUDE`.

* For a :blue:`reused concept`, the :blue:`Supervisor` checks that:

  * the name of the product concept is already well allotted.
  * the operator is allowed to accept reused concepts,
  * the type of the concept conforms to the type of concept produced by the operator.

**Example**

.. code-block:: python

  DEBUT()
  concept = operator()                 # (1) correct: we define the concept
  concept = operator()                 # (2) incorrect: we are trying to redefine the 
                                       #     concept without being explicit
  concept = operator(reuse = concept)  # (3) correct, if the operator accepts
                                       #     existing concepts and if the type is 
                                       #     coherent; it is incorrect if the operator
                                       #     does not accept them.
  FIN()

.. note::

  The user can destroy a concept and reuse its name.


3.5 Body of a command
---------------------

3.5.1 Introduction
^^^^^^^^^^^^^^^^^^

The body of a command contains the "variable" part of the command.  Statements are separated by commas 
and apart from the :red:`reuse` attribute mentioned earlier, they are all of the shape::

  [keyword] = [argument]

The keyword is necessarily a keyword of the current command, declared in the dictionary of the command.

3.5.2 Keyword
^^^^^^^^^^^^^

A keyword is a formal identifier, it is the name of the attribute receiving the argument.

Example::

   MATRIX = ...

.. admonition:: Syntactic remarks

   * the order of appearance of the keywords is free, it is not imposed by the order of declaration
     in the dictionary
   * the keywords cannot exceed 16 characters (but only the first 10 characters are significant)

There are two types of keywords: simple keywords and keyword factors factors which differ in
their arguments.

3.5.3 Argument of a simple keyword
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.5.3.1 The type of the arguments
+++++++++++++++++++++++++++++++++

The basic types recognized by the ``Aster`` supervisor are:

  * integers
  * reals
  * complex numbers
  * text strings
  * logical numbers
  * ``Aster`` concepts

as well as the lists of these types.

Integers and reals correspond exactly to equivalent types in Python.

Examples of keywords expecting real and integer values:

============================================ ===============================
  Dictionary                                   Command File
============================================ ===============================
  ``VALE = SIMP(statut = 'f', type = 'R')``  ``VALE = 10``
  ``INFO = SIMP(statut = 'f', type = 'I')``  ``INFO = 1``
============================================ ===============================

A complex type is represented as a Python "tuple" containing a character string indicating the
mode of representation of the complex number (real-imaginary or modulus-phase), and then the
numeric values. 

=============================================== ===================================
  Dictionary                                     Command File
=============================================== ===================================
  ``VALE_C = SIMP (statut = 'f', typ = 'C')``   ``VALE_C = ('RI', 0.732, -0.732)``

                                                ``VALE_C = ('MP', 1.0 , -45.0)``
=============================================== ===================================

The two notations are strictly equivalent. In notation 'MP', the phase is in degrees.

The text type is declared between single quotes. The case is respected. However, when a keyword
must take a value from a predefined list in the dictionary, usage dictates that this value be
in upper case.

==================================================== ===================================
  Dictionary                                           Command File
==================================================== ===================================
 ``ALL = SIMP (typ = 'TXM', into = ('YES', 'NO'))``  ``ALL = 'YES'``
==================================================== ===================================

Case is important, and in the above context the following command line will fail::

  ALL = 'yes'

The logical type is not used in the command dictionary.

The concept is declared simply by its name, without dimensions or quotation marks.


3.5.3.2 Concept of list
+++++++++++++++++++++++

.. index:: lists

.. caution::

  The word "list"is an abuse language here. It is not the Python "list" type but rather tuples, within the
  meaning of Python: the difference is that items are declared between an opening bracket and a 
  closing bracket and separated by commas.

Lists are homogeneous, i.e. all elements are of the same base type. Any base type can
be used in a list.  Examples:

.. code-block:: python

   a = (1, 2, 3, 4)  # list of integers
   b = ('this', 'is', 'a', 'list', 'of', 'strings')  # list of text strings
   c = (result1, result2, result3) # list of concepts

   d = (1, 3, 4.0)  # incorrect list (not homogeneous)
                    # Heterogeneous list of integers and reals

A list containing only one element can be described without brackets.


3.5.4 Factor keyword
^^^^^^^^^^^^^^^^^^^^

.. index:: simple keyword
.. index:: keyword factor

Some information cannot be given globally (all at once in the command). It is therefore important 
to provide for the repetition of certain keywords, to be able to assign them different arguments. 
The :blue:`keyword factor` offers this possibility. 

In a :blue:`keyword factor`, one will find a set of keywords (simple), which may be used with 
each occurrence of the :blue:`keyword factor`. This also improves the readability of the command 
file by grouping together keywords that share a common meaning: for example the different parameters 
of the same material.

Contrary to the :blue:`simple keyword`, the :blue:`keyword factor` can receive only one type of object: 
the supervisory object ``_F``, or a list of these objects.

When there is only one appearance of the :blue:`keyword factor`, we can write:

.. code-block:: python

   IMPRESSION = _F(RESULT = result, UNIT = 6),
   ...

or

.. code-block:: python

   IMPRESSION = ( _F(RESULT = result, UNIT = 6), ),
   ...

In the first case, the keyword factor ``IMPRESSION`` receives an object ``_F``.  In the other, it 
receives a singleton. 

.. note::

  Please pay attention to the comma; in Python, a tuple with an element is written: ``(element,)``

If the keyword factor has several occurrences, two for example, we can write:

.. code-block:: python

   IMPRESSION = (
                 _F (RESULT = result1, UNIT = 6),
                 _F (RESULT = result2, UNIT = 7) ),
   ...

The number of occurrences (minimum and/or maximum) expected of a keyword factor is defined in the
command dictionary.

**Default values and concepts**

.. index:: default value

It is possible to have the :blue:`Supervisor` assign default values. These values defined
in the commands dictionary and not in FORTRAN. 

From the point of view of the routine associated with 
the command, there is not distinction  between a value provided by the user and a default value 
introduced by the :blue:`Supervisor`. This appears when the :blue:`Supervisor` prints user commands 
in the message file: all default values appear in the command text, if they have not been supplied by
the user.

.. admonition:: Reminder

   One cannot give a default value to a concept.

4 Defining values and evaluating expressions
=============================================

It is possible to assign values to Python variables in order to use them as arguments of simple keywords.
These variables are called :red:`parameters` in ``EFICAS``. They may contain integer, real, complex values, 
text strings, or lists of these types.

.. code-block:: python

   young    = 2.0e+11
   material = DEFI_MATERIAU(ELAS = _F(E = young, NU = 0.3))

At the end of the execution, the Python context is saved in the database. So in a ``POURSUITE`` 
(:green:`CONTINUE`) step that follows, the parameters will be present with their predefined values, 
just like the ``Aster`` concepts.

It is possible to perform Python operations on arguments of simple keywords:

.. code-block:: python

   pi_by_2 = pi / 2.0
   comm    = MY_COMMAND(VALE = pi_by_2)

or

.. code-block:: python

   var  = 'world'
   comm = MY_COMMAND(VALE  = pi_by_2,
                     VALE2 = pi_by_2 + cos(30.0),
                     TEXT  = 'hello' + var)


5 Using Python in the command files
====================================

It is not necessary to know the Python language to use ``Code_Aster``. Indeed, with the help of some
basic rules on the indentation and parenthesizing, only knowledge of the ``Aster`` command language 
described in the command dictionary is necessary.  Also, ``EFICAS`` makes it possible to
avoid direct reference to the dictionary or command syntax by graphically proposing keywords.

However, the advanced user will be able to use  the power of the Python language in the
command file, since the commands are already written in this language.

The four main uses can be: writing custom macro-commands, using general python instructions, 
importing useful python modules, retrieving information from ``Code_Aster`` data structures in Python variables.

.. note::

  If one wants to use accented characters in the command file or the imported modules, it is necessary 
  to place the following instruction in first or second-row at the top of the file::

     #–*–coding: iso-8859-1–*

  The absence of this line causes an error. 

5.1 Custom Macro-commands
-------------------------

See document **[D5.01.02]**: "Create a new macro-command".
Custom macro commands are easy to program. They can be used to capitalize on recurring calculation 
schemes and thus constitute a business tool.  It is strongly advised to take as a model 
existing macro-orders, such as the package ``Macro`` in the ``bibpyt`` directory on the 
``Code_Aster`` installation.

5.2 General instructions for Python and useful modules
------------------------------------------------------

Advanced users can benefit greatly from the use of loops (``for``), tests (``if``), 
exceptions (``try``, ``except``) and, generally, from the power of the Python language in
their command files. in general with all the power of the PYTHON language directly in their batch file. 

Many examples are present in the test cases of the ``Code-Aster`` test database. For example, we can
adapt the mesh by placing the calculation sequence/remeshing in a loop, establish a criterion for 
stopping the iterations by a test on a calculated value, etc.  For more detail on exception handling, consult
:ref:`python_exceptions`.

In a loop, if an already existing concept is recreated, it is necessary to destroy it first using the
``DETRUIRE`` (:green:`DESTROY`) command. 

Other Python features of interest to the user of ``Code_Aster`` are:

* read-write from/to files
* numerical calculation (for example using Numerical Python)
* calls via the ``os`` module to launch third party codes (``os.system``)
* manipulation of strings
* calls to graphics modules (``grace``, ``gnuplot``)

.. _python_exceptions:

5.3 Python exceptions in ``Code_Aster``
-----------------------------------------

The Python exception mechanism of uses ``try-except`` blocks to catch internal exceptions
in ``Code_Aster``.  Internal exceptions are raised if the code fails for some reason.
The exception mechanism prevents crashes in the event of a failure.  An example of the
syntax is::

  try:
      block of instructions 
  except IdentifiedError, message:
      block of instructions executed if IdentifiedError occurs 

In the command file, we can use this mechanism with any exception defined in standard Python.
``Code-Aster`` also raises some special exceptions.  These are divided into two categories:
exceptions associated with ``<S>`` errors, and those associated with ``<F>`` (fatal) errors.

5.3.1 Handling of <S> errors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the event of an ``<S>`` error, the following exceptions are thrown:

========================================  ==========================================================
``aster.NonConvergenceError``             The outer Newton iterations have not converged
``aster.EchecComportementError``          Problem with integrating a material model
``aster.BandeFrequenceVideError``         No modes found in the frequency band specified
``aster.MatriceSinguliereError``          Singular matrix
``aster.TraitementContactError``          Problem during contact processing
``aster.MatriceContactSinguliereError``   Singular contact matrix
``aster.ArretCPUError``                   Allocated CPU time exceeded
========================================  ==========================================================

All these exceptions are derived from the base exception ``aster.error``, i.e., if you catch
``aster.error`` then all the exceptions above will be caught.  Nevertheless, it is always preferable
to be more specific about the exception to be caught.

**Example of use**

.. code-block:: python

  try:
      result = STAT_NON_LINE('...')

  except (aster.NonConvergenceError, message):
      print("Stopping because: %s" % str(message))

      # We know that it takes a lot more iterations to converge
      print "Increasing the iteration count."
      result = STAT_NON_LINE (reuse = result,
                              '...',
                              CONVERGENCE = _F(ITER_GLOB_MAXI = 400,),)

  except (aster.error, message):
      print("Another error occurred: %s" % str(message))
      print("Stop")

      from Utilitai.Utmess import UTMESS
      UTMESS('F', 'Example', 'Unexpected error <S>')

5.3.2 Handling of <F>atal errors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the event of a fatal error, the behavior can be modified by the user. By default, the code stops if 
error ``<F>`` occurs.  One can check the error report (output of FORTRAN routine calls) in the output 
file.  The code throws an ``aster.FatalError`` exception.  The user can take back control by
catching the exception.  Otherwise, the code halts.  

This choice is determined in the commands ``DEBUT/POURSUITE`` by modifying the arguments of
keyword ``ERROR`` (cf :ref:`[U4.11.01] <u4.11.01>` and **[U4.11.03]**).  Alternatively, we can use the 
``aster.onFatalError`` method to regain control.  

When called without an argument, ``aster.onFatalError`` returns the latest action in the event of fatal error:

.. code-block:: python

   current_action = aster.onFatalError()
   print("**Fatal error** Current action : %s" % current_action)

This allows the user to define the behavior that is appropriate:

.. code-block:: python

   aster.onFatalError('EXCEPTION')   # one raises the exception FatalError

or

.. code-block:: python

   aster.onFatalError('ABORT')       # one stops with error feedback

5.3.3 Validity of concepts in the event of throwing an exception
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When an exception is raised and caught (by ``except``), the concept produced by the command
where the error occurred is returned as is, the command not having completed normally.
In certain cases, e.g., after a fatal error, it is possible that the product concept is not usable. 
Use ``DETRUIRE`` (:green:`DESTROY`) to delete it.  Similarly, if you want to reuse the name of the 
concept to create a new one, you have to ``DETRUIRE`` the one obtained in the ``try`` block.

In the event of error ``<S>`` being raised in ``STAT_NO_LINE``/``DYNA_NON_LINE``, the concept is 
still valid, and can be reused (keyword ``reuse``) to continue calculation with another strategy 
(as in the example in the previous subsection). 

Finally, before returning control to the user, the objects of the volatile database are deleted by 
a call to ``JEDETV``, and the Jeveux pointer is repositioned to 1 (with ``JEDEMA``) to release the 
returned objects in memory.
 

5.3.4 Precautions while using exceptions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The two exceptions ``aster.error`` and ``aster.FatalError`` are independent, which means that if you want 
to take control in the event of an ``<S>`` error or an ``<F>`` error:

* you must activate the throwing of the exception in the event of an ``<F>`` error with
  ``aster.FatalError('EXCEPTION')`` or in ``DEBUT``/``POURSUITE`` (:green:`BEGIN/CONTINUE`).
* you must catch the two exceptions with ``except(aster.FatalError, aster.error), message: ...``.

  It is not recommended to use ``except:`` without specifying which exception is expected. Indeed, 
  the processing carried out in the ``except`` block is unlikely to be valid in all error cases.
  Likewise, as in the example given earlier, it is preferable to use the specific exceptions
  ``NonConvergenceError``, ``ArretCPUError``, etc.,  than the base level exception ``aster.error``.
  The idea is to determine exactly what caused the exception.

5.4 Recovery of computed values in Python variables
---------------------------------------------------

To power of Python can be exploited to conditionally launch actions based on what the code has calculated
so far.  Some interfaces exist between Python and ``Aster`` data structures present in Jeveux managed memory. 
This is an evolving domain and future developments are expected.

It is essential to understand that recovering calculated data requires that the instructions that are
needed to generate these data have been carried out beforehand. In other words, it is essential
to execute the code in the mode ``PAR_LOT = 'NON'`` (keyword of the command ``DEBUT``).  In that case, there 
is no global analysis of the command file, each instruction is executed sequentially.  When the computation
arrives at an instruction, all the concepts it preceding have already been calculated.

Some methods that can be used to access the data structures are listed below (for more detail, see
**[U1.03.02]**).

======================= ==================== ==================== =========================
Data structure          Method               Python type          Information
======================= ==================== ==================== =========================
listr8                  LIST_VALEURS         list                 List of values
mesh                    LIST_GROUP_NO        list                 List of node groups
                        LIST_GROUP_MA        list                 List of element groups
table                   [...]                real                 Contents of the table
function                LIST_VALEURS         list                 List of values
result                  LIST_CHAMPS          list                 List of fields
                        LIST_NOM_CMP         list                 List of components
                        LIST_VARI_ACCES      list                 List of access variables
                        LIST_PARA            list                 List of parameters
field_node              EXTR_COMP            post_comp_cham_no    Content of field in table
field_elem              EXTR_COMP            post_comp_cham_el    Content of field in table
Any Jeveux object       getvectjev           list                 List of Jeveux vectors
======================= ==================== ==================== =========================


5.5 Example: Dynamic construction of keywords
---------------------------------------------

In the case of a repetitive keyword argument, the user may want to compose its content using
a Python script, e.g., in a list or dictionary which it will then provide to the keyword argument. 
The example below shows three ways to write the same keyword argument.

.. code-block:: python

   DEBUT(PAR_LOT='NON')

   style_1 = [ _F(JUSQU_A = 1., PAS = 0.1),
               _F(JUSQU_A = 2., PAS = 0.1),
               _F(JUSQU_A = 3., PAS = 0.1) ]

   style_2 = [ _F(JUSQU_A = float (i), PAS = 0.1) for i in range (1, 4) ]

   dict_3  = [ {'JUSQU_A' : float (i), 'PAS' : 0.1} for i in range (1, 4) ]
   style_3 = [ _F(**args) for args in dict_3]

   list_1 = DEFI_LISTE_REEL( DEBUT = 0.,
                             INTERVALLE = style_1 )

   list_2 = DEFI_LISTE_REEL( DEBUT = 0.,
                             INTERVALLE = style_2 )

   list_3 = DEFI_LISTE_REEL( DEBUT = 0.,
                             INTERVALLE = style_3 )
   FIN()

