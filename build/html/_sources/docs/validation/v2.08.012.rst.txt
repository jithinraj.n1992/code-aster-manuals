.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v2.08.012:

**************************************************************************************
**V2.08.012**: FORMA12 - Linear and nonlinear dynamic analysis of a cantilevered plate
**************************************************************************************

This tutorial is an introduction to the use of ``Code_Aster`` for transient dynamic analysis
(linear and nonlinear).

It deals with a thick steel plate that is subjected to a sinusoidal excitation on a vibrating
table. The plate is modeled with 3D elements.
We compare a transient analysis that uses a modal basis with a direct transient analysis. In the
last part we take into account the plasticity of steel with a nonlinear model.

The model cases explored in this tutorial are:

* :ref:`v2.08.012.2`
* :ref:`v2.08.012.3`
* :ref:`v2.08.012.4`
* :ref:`v2.08.012.5`
* :ref:`v2.08.012.6`

The first section presents the geometry and data common to all the models.

.. _v2.08.012.1:

Introduction
============

.. _v2.08.012.1.1:

Geometry
--------

We will study the response of a steel plate in this tutorial. The plate is a rectangular
parallelpiped with dimensions: length 2.0 meters, width 2.0 meters and thickness 0.003 meters

.. _fig_v2.08.012.1.1-1:
.. figure:: v2.08.012/fig1_v2.08.012.png
   :height: 300 px
   :align: center

   Plate geometry.


The plate is firmly embedded on one of its sides (ENCAS) and fixed on a vibrating table. We want
to calculate the displacement of a point :green:`P` located on the upper face at a corner of plate.

.. _fig_v2.08.012.1.1-2:
.. figure:: v2.08.012/fig2_v2.08.012.png
   :height: 300 px
   :align: center

   Plate boundary conditions.


.. _v2.08.012.1.2:

Materials
----------

Steel is, for linear analyses, considered as an isotropic, linear elastic material:

* Young modulus: :math:`E` = 200 GPa
* Poisson's ratio: :math:`\nu` = 0.3
* Density: :math:`\rho` = 8000 kg / m :math:`^3`

For the plasticity calculations in Model E, we assume that the elastic-plastic tangent
modulus for linear isotropic hardening snd the yield stress are:

* Hardening modulus: :math:`E^T` = 2 GPa
* Yield stress: :math:`\sigma_y` = 200 MPa

.. _v2.08.012.1.3:

Boundary conditions
-------------------

The plate is embedded on one of its sides.

A distracted engineer made a mistake in the table vibration settings. Instead of the expected
stress (a synthetic accelerogram), he sent a simple sinusoidal signal but of very high amplitude.
The plate was not designed for such a shake and we try to determine the movement that the plate
may have undergone during this unfortunate test.

The input signal is a simple sine wave of 15 Hertz frequency. The amplitude is 30g horizontally
and 30g vertically downward (:math:`g` = 10 m / s :math:`^2`). The duration of the signal is 0.5 s.

.. _v2.08.012.1.4:

Modeling and meshing in Salome-Meca
------------------------------------

.. _v2.08.012.1.4.1:

Using a sub-mesh
^^^^^^^^^^^^^^^^^

In the :blue:`Geometry` module, we use the :red:`New Entity> Primitive> Box` menu to build
a box with the dimensions :math:`2 \times 2 \times 0.03`.

For convenience in specifying
boundary conditions and points of interest for post-processing in ``Code_Aster``, we need to
create some named geometry groups using :red:`New Entity> Group> Create`:

* The plate is fixed on one of the thin faces. Create a group for this face
  called :green:`fixed_face`.
* We will need a point on the upper face, on an edge opposite to the embedded edges, to be used
  later for postprocessing.  Call this point group :green:`point_P`.
* Also add a point group for the origin and call it :green:`point_O`.
* To be able to specify the smoothness of the mesh in the thickness of the plate, it will be
  useful to create a group corresponding to the vertical edge of one of the corners of the plate.
  Call this edge group :green:`edge_L`.

The next step is the creation of the mesh with the :blue:`Mesh` module. Many meshing algorithms
are available in ``Salome-Meca``. To obtain a regular hexadedral mesh, we can use:
:red:`3d - Hexahedron (i, j, k) / 2d - Quadrangle (Mapping) / 1d - Wire discretization`.
Set the number of segments for the 1d mesh to 20.

To avoid a fine mesh through the thickness of the plate, create a **sub-mesh**
(:red:`Create Submesh`) on the
corner edge, :green:`edge_L`, with 2 segments and "propagate" the discretization to opposite
edges by choosing the option :blue:`Propagation of 1D Hyp. on opposite edges` in
:blue:`Add. Hypothesis`.

As a compromise between calculation time and precision, we can create 20 elements in the width of
the plate and 2 in the thickness. We then obtain a coarse mesh after running :blue:`Compute mesh`
but this defect can be compensated by converting the elements from linear to quadratic:
:red:`Modification> Convert to / from quadratic`. At the end of the procedure, the mesh should
contain 4725 nodes and 800 3D elements.

.. figure:: v2.08.012/fig3_v2.08.012.png
   :height: 300 px
   :align: center

   Mesh of the plate.

The Python script for creating this model is :download:`here <v2.08.012/forma12_submesh.py>`).  The annotated script is listed below.

.. include::  v2.08.012/forma12_submesh.py
     :literal:

.. _v2.08.012.1.4.2:

Using extrusion
^^^^^^^^^^^^^^^^^

Alternatively, we can mesh the fixed face using a 2D Medial-axis algorithm with 20 segments and
2 layers, and then extrude the 2D mesh in the y-direction in 20 steps.

The Python script for creating the extruded mesh and the associated groups of nodes and elements
is :download:`here <v2.08.012/forma12_extrude.py>`).  The annotated script is listed below.

.. include::  v2.08.012/forma12_extrude.py
     :literal:


.. _v2.08.012.2:

**Model A** : Modal analysis
============================

.. _v2.08.012.2.1:

Objective
----------

To get an idea of ​​the dynamic properties of the system, we start with a modal analysis in
``AsterStudy``.

.. _v2.08.012.2.2:

Modal analysis with Code_Aster
------------------------------

To calculate the first few modes in with ``Code_Aster``, the simplest tool is probably the assistant
available in ``Salome-Meca``.  This assistant can be accessed via
:red:`Operations -> Add Stage with Assistant -> Modal analysis`.

You can then set up the simulation by filling in a sequence of dialog boxes. In this particular case,
you have the choice of selecting the number of modes or a frequency interval for the modal
analysis.  For the plate problem, you can either ask for 4 modes or the modes up to a frequency of
50 Hz. This is a prudent choice since the excitation frequency is only 15 Hz.

The main commands needed are:

::

   # Assemble system
   ASSEMBLAGE(CHAM_MATER = mater,
              CHARGE = disp_bc,
              MATR_ASSE = (_F(MATRICE = CO('stif_mat'),
                              OPTION = 'RIGI_MECA'),
                           _F(MATRICE = CO('mass_mat'),
                              OPTION = 'MASS_MECA')),
              MODELE = model,
              NUME_DDL = CO('num_dof'))

   # Compute modes
   result = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 50.0)),
                       MATR_MASS = mass_mat,
                       MATR_RIGI = stif_mat,
                       OPTION = 'BANDE')

   # Compute normalized modes (using geneeralized mass)
   result = NORM_MODE(reuse = result,
                      MODE = result,
                      NORME = 'MASS_GENE')


The command file for running this model is :download:`here <v2.08.012/forma12a.comm>`).  The annotated script is listed below.

.. include::  v2.08.012/forma12a.comm
     :literal:

.. _v2.08.012.2.3:

Post-processing
----------------

Post-processing using the :blue:`Results` option in ``AsterStudy`` can be initiated by
right clicking on the results file created with :red:`IMPR_RESU`, and selecting :blue:`Post-process``.
Alternatively, you can use ``ParaVis``.  However, bugs in recent implementations of ParaVis
may require you to convert the data into ``VTU`` format using a tool such as ``meshio`` before
you can visualize separate time steps (modes).

The modal shapes and frequencies are shown in the figures below.

.. list-table::

   * - .. figure:: v2.08.012/forma12a_1.png

          Mode 1 - 6.2887 Hz

     - .. figure:: v2.08.012/forma12a_2.png

          Mode 2 - 15.3529 Hz

   * - .. figure:: v2.08.012/forma12a_3.png

          Mode 3 - 38.5486 Hz

     - .. figure:: v2.08.012/forma12a_4.png

          Mode 4 - 49.0722 Hz

To check the quality of the mesh, one can perform a convergence study by refining the mesh. For
the simple geometry of the plate, it is possible to find a good approximation by an analytical
formula (e.g., Formulas for Stress, Strain, and Structural Matrices, Walter D. Pikey - ed.
John Wiley & Sons, Inc. 1994).

For the case studied, we find: :math:`f_1` = 6.23 Hz and :math:`f_3` = 38.16 Hz.  The results
from ``Code_Aster`` are thus quite accurate, in spite of the coarseness of the mesh.  With a linear
mesh, the results would not have been so good.


.. _v2.08.012.3:

**Model B**: Direct transient analysis in the physical basis
=============================================================

.. _v2.08.012.3.1:

Problem and sketch of the calculation
-------------------------------------

We now want to calculate the movement of the plate on its vibrating table.

We start with the most intuitive calculation, a direct calculation on a physical basis.
For the purposes of this calculation we will use time increments of :math:`t_\text{inc}` = 0.002 s
and run the simulation up to :math:`t_\text{final}` = 0.5 s.  The excitation frequency is
:math:`f_c` = 15 Hz, which is equivalent to an angular frequency of
:math:`\omega = 2\pi f_c`.  We will target a frequency band of :math:`f_b = 1/(5 * t_\text{inc})`
= 100 /s.

We can use ``AsterStudy`` to create the command file.  The main steps are:

* Read of the MED mesh (:red:`LIRE_MAILLAGE`)
* Define the finite element type (:red:`AFFE_MODELE`) and use 3D elements (:blue:`3D`)
* Define and assign the material (:red:`DEFI_MATERIAU` and :red:`AFFE_MATERIAU`).  You
  will need to define :blue:`RHO` so that the mass matrix can be computed.
* Assign the boundary conditions (:red:`AFFE_CHAR_MECA`):

  * Constrain the face (:green:`fixed_face`) with :blue:`DDL_IMPO` (fix the displacement 
    DOFs)
  * Apply the gravity acceleration load :
    :red:`AFFE_CHAR_MECA` :blue:`(PESANTEUR = _F(GRAVITE = 300., DIRECTION = (- 1,0,1))`

* Set up a sinusoidal scaling function for the gravity load:

  ::

    sin_om = FORMULE(VALE = 'sin(omega*INST)',
                     omega = omega,
                     NOM_PARA = 'INST')
     
* Perform the transient calculation:

  ::

    result = DYNA_LINE(TYPE_CALCUL = "TRAN",
                       BASE_CALCUL = "PHYS",
                       MODELE = model,
                       CHAM_MATER = mater,
                       CHARGE = disp_bc,
                       EXCIT = _F(CHARGE = gravity, FONC_MULT = sin_om),
                       BANDE_ANALYSE = freq_band,
                       SCHEMA_TEMPS = _F(SCHEMA = 'NEWMARK'),
                       INCREMENT = _F(INST_FIN = t_final,
                                      PAS = t_inc))
     
* For plotting displacements as a function of time, extract data from point :blue:`point_P` with
  :red:`RECU_FONCTION` and save the data to a table :red:`IMPR_FONCTION`.

The command file can be found at :download:`forma12b.comm <v2.08.012/forma12b.comm>`.
       
.. _v2.08.012.3.2:

Results
-------

Let's observe the displacement curves:

.. image:: v2.08.012/forma12b_29.svg
   :height: 300 px
   :align: center

A sinusoidal response is observed at the excitation frequency of 15 Hz. This sinusoidal response is
nevertheless strongly modulated by the response of the first eigen mode (at 6.3 Hz) of the plate.

.. raw:: html

    <video width="320" height="240" controls>
      <source src="../../_static/v2.08.012/forma12b.ogv" type="video/ogg">
      Your browser does not support the video tag.
    </video>


.. _v2.08.012.4:

**Model C**: Transient analysis in the modal basis
==================================================

.. _v2.08.012.4.1:

Problem and tips
-----------------

Transient analysis can also be done more efficiently by projecting the problem on to a modal
basis. In this example, we will compare this method with a direct transient computation.

.. important::

   For modal superposition, a rule of thumb is that the modes used as the basis should cover
   a range of frequencies that is more than twice as large as the excitation frequency.  In this
   example, the excitation frequency is 15 Hz.  Hence, we should include modes at least as large as
   30 Hz.

With DYNA_LINE
^^^^^^^^^^^^^^

The procedure is similar to that of :ref:`Model B <v2.08.012.3>` except that will use
:red:`DYNA_LINE` with a generalized eigen basis in this example, instead of a physical basis.

As before :blue:`TYPE_CALCUL = 'TRAN`.  However, in
this case we use the generalized eigen basis when we specify :blue:`BASE_CALCUL = 'GENE'`.
If you want to save the data in the modal basis, you can specify concept names as arguments
to :blue:`BASE_RESU` and :blue:`RESU_GENE`.  We will not do a static correction in this case
and set :blue:`ENRI_STAT = 'NON'`.  The analysis interval will be limited from 0 Hz to 50 Hz,
with :blue:`BANDE_ANALYSE = 50`.  The complete :red:`DYNA_LINE` command is listed below.

::

   res_gen = DYNA_LINE(TYPE_CALCUL = "TRAN",
                       BASE_CALCUL = "GENE",
                       BASE_RESU = CO('res_base'),
                       RESU_GENE = CO('tran_gen'),
                       ENRI_STAT = 'NON',
                       MODELE = model,
                       CHAM_MATER = mater,
                       CHARGE = disp_bc,
                       EXCIT = _F(CHARGE = gravity,
                                  FONC_MULT = sin_om),
                       BANDE_ANALYSE = 50,
                       SCHEMA_TEMPS = _F(SCHEMA = 'NEWMARK'),
                       INCREMENT = _F(INST_FIN = t_final,
                                      PAS = t_inc))

After the calculation is complete,                                      

* Extract the displacement at point :green:`point_P` with :red:`RECU_FONCTION` making sure that
  the result type is :blue:`RESULTAT`
* Save the displacement as a function of time with :red:`IMPR_FONCTION`

You can also do the same calculation with manual timestepping by dividing the total time
interval of 5 sec into 100 intervals.  The main steps in that process are:

* Create a list of times with the finer timestep: :red:`DEFI_LIST_REEL`
* Set an interpolation function that computes the value of the load at each of the instants
  of time in the fine time list: :red:`CALC_FONC_INTERP`
* Use the interpolation function to specify the load multiplier in :red:`DYNA_LINE`. 

The command file for this case (including the computation in the physical basis) can be
found in :download:`forma12c.comm <v2.08.012/forma12c.comm>`.


With DYNA_VIBR
^^^^^^^^^^^^^^
Alternatively, to save time, if we save modal solution in :ref:`Model A <v2.08.012.2>` in an ``Aster``
basis, we can avoid performing the modal computation again and start directly in continuation mode
with :red:`POURSUITE`.  

For an analysis that uses pre-computed modes, we need to project the stiffness and mass matrices, and
the displacement and force vectors into a modal basis using :red:`PROJ_BASE` and the modes calculated,
for example, in Model A.  The main steps of the process are:

* Recovery of the results of Model A: :red:`POURSUITE`
* Definition of the gravity acceleration: :red:`AFFE_CHAR_MECA`
  :blue:`(GRAVITY = _F(GRAVITY = 300., DIRECTION = (- 1,0,1))`
* Calculate a set of vectors for the gravity load that can be assembled: :red:`CALC_VECT_ELEM`
* Assemble the load vector: :red:`ASSE_VECTEUR`
* Projection the mass ans stiffness matrices, and the loading vector to the modal basis:
  :red:`PROJ_BASE`
* Set up the sine function for scaling the load as a function of time
  (sine of period :math:`15 \times 2\pi` s) using :red:`FORMULE`
* Define of a list of times (e.g., 0.5 s with a timestep of 0.002 s): :red:`DEFI_LIST_REEL`
* Do the transient calculation on the modal basis:
  :red:`DYNA_VIBRA` :blue:`(TYPE_CALCUL = 'TRANS', BASE_CALCUL = 'GENE')`
* Extract the displacement at point :green:`point_P` with :red:`RECU_FONCTION` making sure that
  the result type is **not** :blue:`RESULTAT` but :blue:`RESU_GENE`
* Save the displacement as a function of time with :red:`IMPR_FONCTION`

.. _v2.08.012.4.2:

Results
-------

For vertical, :math:`u_z`, displacement our results are close to those from a direct transient
computation. But if we examine the displacements in the horizontal direction, :math:`u_x`, they are
quite different unless the timestep is refined by a factor of 100!!! Although the modal basis was
calculated beyond 45 Hz and the rule of thumb for modal superposition was respected, in the
horizontal direction, the modal projection method produces results that are quite different from
the direct result.

.. image:: v2.08.012/forma12c_29.svg
   :height: 300 px
   :align: center


What happened? The plate is in fact very stiff in its plane (horizontal direction). The eigen modes
that we have used are not sufficient to represent its movement accurately. To compute the
displacement correctly, it would be necessary to use many more eigen modes.

However, one method is much more efficient. This is the method of "static correction", which makes
it possible to take into account the "static" contribution of the structure to the movement. Model D
gives an example of that approach.


.. _v2.08.012.5:

**Model D** : Modal superposition with a-posteriori static correction
=====================================================================

.. _v2.08.012.5.1:

Calculation procedure
----------------------

The steps for this method are quite close to the one without any static correction. To demonstrate
an older approach for dynamics analysis, we will use :red:`DYNA_VIBRA` in this case instead of
:red:`DYNA_LINE`.

The main steps are quite similar to the previous cases, except that we explicitly run a modal analysis
first.

* Start by initializing the parameters of the model and create functions for computing
  the load curve and its first and second derivatives.  If we want an approximation of the
  derivatives we can use :red:`CALC_FONCTION` (this is useful when an analytical load curve is
  not available).

  ::

     sin_om = FORMULE(VALE = 'sin(omega*INST)',
                      omega = omega,
                      NOM_PARA = 'INST')

     dsin_om = FORMULE(VALE = 'omega*cos(omega*INST)',
                       omega = omega,
                       NOM_PARA = 'INST')

     d2sin_om = FORMULE(VALE = '-omega**2*sin(omega*INST)',
                        omega = omega,
                        NOM_PARA = 'INST')

* Read the mesh with :red:`LIRE_MAILLAGE`
* Assign a 3D mechanical model to the mesh with :red:`AFFE_MODELE`
* Define and assign the material with :red:`DEFI_MATERIAU` and :red:`AFFE_MATERIAU`
* Apply the displacement boundary conditions with :red:`AFFE_CHAR_MECA` (:blue:`DDL_IMPO`) and
  fix the face :green:`fixed_face`

* Modal analysis:

  * Assemble the system of equations for the eigenvalue problem with the macro :red:`ASSEMBLAGE`, and
    save the results in :green:`stif_mat`, :green:`mass_mat`, and :green:`num_dof`.
  * Compute the modes in the frequency interval 0 to 50 Hz with :red:`CALC_MODES`.
  * Normalize the modes using the generalized mass with :red:`NORM_MODE` (:blue:`NORME = 'MASS_GENE'`).
  * Extract modes that lie between 0 Hz and 60 Hz using :red:`EXTR_MODE`.

* Transient analysis (with static correction):
  
  * Set up the gravity acceleration load with :red:`AFFE_CHAR_MECA`
    :blue:`(GRAVITY = _F (GRAVITY = 300., DIRECTION = (- 1,0,1))`
  * Convert gravity load into element vectors with :red:`CALC_VECT_ELEM`
  * Assemble the load vector with :red:`ASSE_VECTEUR`
  * **Compute the static correction**: :red:`MACRO_ELAST_MULT`
  * Project the mass and stiffness matrices and the loading vector: :red:`PROJ_BASE`
  * Do transient calculation using the modal basis: :red:`DYNA_VIBRA`
    :blue:`(TYPE_CALCUL = 'TRANS',BASE_CALCUL = 'GENE')`. Add the static deformation
    corrected gravity load to the analysis with :blue:`MODE_CORR` and the projected
    gravity load as the excitation in :blue:`EXCIT` with :blue:`FONC_MULT = sin_om`,
    :blue:`D_FONC_DT = dsin_om` and :blue:`D_FONC_DT2 = d2sin_om`, the first and second derivatives.
  * Extract the displacement at the point :green:`point_P` with :red:`RECU_FONCTION` :blue:`(RESU_GENE)`.
    If you don't add :blue:`CORR_STAT = 'OUI'` to the :red:`RECU_FONCTION` command, the corrected
    displacements will not be saved.
  * Save the results to a file with :red:`IMPR_FONCTION`

.. warning::

   Do not forget to include the correction when returning to the physical basis.

The command file for this case can be found in :download:`forma12d.comm <v2.08.012/forma12d.comm>`.

.. _v2.08.012.5.2:

Results
--------

.. image:: v2.08.012/forma12d_29.svg
   :height: 300 px
   :align: center

Thanks to the static correction, the results are much closer to those from the direct
calculation.


.. _v2.08.012.6:

Model E : Non-linear dynamics with von Mises isotropic hardening plasticity
===========================================================================

.. _v2.08.012.6.1:

Goal
-----

We now seek to determine the damage to the plate caused by the excessive amplitude of the
acceleration set point. A linear hardening von Mises plasticity model is used to estimate the
residual strain, following the unfortunate test.

.. _v2.08.012.6.2:

Calculation procedure
----------------------

To calculate the dynamic non-linear behavior, we will use the operator :red:`DYNA_NON_LINE`.  This
command is similar to :red:`STAT_NON_LINE`. The main difference is in the method of time integration
and the influence of inertia (mass matrix). To compare the linear and non-linear results, one can
start from Model B.

The initial commands (reading the mesh, definition of the model, materials etc.) are the same as
for the previous cases. The main  differences are: 

* Define the plasticity model in :red:`DEFI_MATERIAU` with :blue:`ECRO_LINE` (linear hardening).
  We use :math:`E^T = E/100` and :math:`\sigma_y` = 200 MPa.

  ::

     steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                    NU = nu,
                                    RHO = rho),
                          ECRO_LINE = _F(D_SIGM_EPSI = E_T,
                                         SY = sig_y))

* Define of a list of times (we can do a computation for 0.5 s with a step of 0.002 s):
  :red:`DEFI_LIST_REEL` then use that list to define a set of time steps with
  :red:`DEFI_LIST_INST`.
* Compute the transient solution: :red:`DYNA_NON_LINE` (one can use the displacement
  controlled :blue:`NEWMARK` method)

   * In :red:`DYNA_NON_LINE` we specify an incremental behavior with an small strain
     elasticity model limited by a Von Mises yield criterion:

     ::

        COMPORTEMENT = _F(RELATION = 'VMIS_CINE_LINE',
                          DEFORMATION = 'PETIT',
                          TOUT = 'OUI')

    * To facilitate convergence, one can experiment with the number of iterations before the
      Newton-Raphson linearization procedure fails to converge:

      ::

         CONVERGENCE = _F(ITER_GLOB_MAXI = 30)

      If the algorithm still does not converge, the time step will have to be reduced.

    * To control the time step size, we must create a heuristic for the time step size using
      :red:`DEFI_LIST_INST`. If the convergence criteria are not achieved with a given time step
      size, the time step  will be subdivided into 5 smaller time steps.

      ::

         tim_step = DEFI_LIST_INST(METHODE = 'MANUEL',
                                   DEFI_LIST = _F(LIST_INST = t_list),
                                   ECHEC =_F(SUBD_NIVEAU = 5,
                                             SUBD_PAS_MINI = 1e-5,
                                             SUBD_METHODE = 'MANUEL'))

As in the previous cases, you can extract the displacements at point P with :red:`RECU_FONCTION`
and save the data to a file with :red:`IMPR_FONCTION`.

In addition to the main nonlinear dynamic computation, for comparison, we also calculate the modal
superposition
results in the modal basis with static correction, the physical basis, and also a Ritz basis.
The Ritz basis is set up using:

::

   ritz_bas = DEFI_BASE_MODALE(RITZ = (_F(MODE_MECA = mode_2),
                                       _F(MODE_INTF = grav_mul)),
                               NUME_REF = num_dof)

The command file for this case can be found in :download:`forma12e.comm <v2.08.012/forma12e.comm>`.

.. _v2.08.012.6.4:

Results analysis
----------------

.. _v2.08.012.6.4.1:

Calculation process
^^^^^^^^^^^^^^^^^^^

On reading the message file, we observe that the number of iterations increases when the
material enters a non-linear state, in contrast to the linear elastic steps, where the convergence
occurs in a single iteration.  For example from time t = 0.264 s to time 0.266 s :

::

         Time of computation:   2.640000000000e-01 - Level of cutting:  1 
        ---------------------------------------------------------------------
        |     NEWTON     |     RESIDU     |     RESIDU     |     OPTION     |
        |    ITERATION   |     RELATIF    |     ABSOLU     |   ASSEMBLAGE   |
        |                | RESI_GLOB_RELA | RESI_GLOB_MAXI |                |
        ---------------------------------------------------------------------
        |     0          | 2.46339E-11    | 3.78568E-06    |TANGENTE        |
        ---------------------------------------------------------------------

         Criterion (S) of convergence reached (S) 


         Time of computation:   2.660000000000e-01 - Level of cutting:  1 
        ---------------------------------------------------------------------
        |     NEWTON     |     RESIDU     |     RESIDU     |     OPTION     |
        |    ITERATION   |     RELATIF    |     ABSOLU     |   ASSEMBLAGE   |
        |                | RESI_GLOB_RELA | RESI_GLOB_MAXI |                |
        ---------------------------------------------------------------------
        |     0        X | 2.63818E-02  X | 4.62989E+03    |TANGENTE        |
        |     1        X | 1.82440E-04  X | 3.20179E+01    |TANGENTE        |
        |     2          | 4.49809E-10    | 7.89407E-05    |TANGENTE        |
        ---------------------------------------------------------------------

In some cases, the algorithm may not converge before the 30 allowed iterations. That provokes a
subdivision of the time step. The total CPU time increases. One can compare the computation times
required by :red:`DYNA_VIBRA` and :red:`DYNA_NON_LINE`. For example we find 10s for :red:`DYNA_VIBRA`
:blue:`(TYPE_CALCUL = 'TRAN',BASE_CALCUL = 'PHYS')` and 550 s for :red:`DYNA_NON_LINE`.

.. _v2.08.012.6.4.2:

Analysis of the displacement curve
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is observed that, compared to the elastic calculation:

* plasticity "dampens" the amplitude of the response
* the apparent frequency of the structure decreases slightly.

.. image:: v2.08.012/forma12e_30.svg
   :height: 300 px
   :align: center

These observations are a sign of the softening of the structure under the effect of plasticity.

The von Mises stress at point O, follows the trend shown in the figure below.

.. image:: v2.08.012/forma12e_30_sig.svg
   :height: 300 px
   :align: center

The motion of the plate is also informative, including the deformation of the fixed side of the
plate.

.. raw:: html

    <video width="320" height="240" controls>
      <source src="../../_static/v2.08.012/forma12e.ogv" type="video/ogg">
      Your browser does not support the video tag.
    </video>

