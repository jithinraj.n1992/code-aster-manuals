#!/usr/bin/env python
import sys
import salome
import GEOM
import SMESH
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import math
import json

ExportPATH="/home/banerjee/Salome/forma11/"

salome.salome_init()
geompy = geomBuilder.New()
smesh = smeshBuilder.New()

# Read mesh
([mesh], status) = smesh.CreateMeshesFromMED(r'/home/banerjee/Salome/forma11/forma11c.mmed')

# Create a profile by selecting nodes by coordinate
profile = mesh.CreateEmptyGroup( SMESH.NODE, 'profile' )

# Create selection box
eps = 1.0e-3
mesh_bb = mesh.BoundingBox()
x_min = 0
x_max = mesh_bb[3] + eps
z_min = mesh_bb[2] - eps
z_max = mesh_bb[5] + eps
y_min = -eps
y_max =  eps

box_min = geompy.MakeVertex(x_min, y_min, z_min)
box_max = geompy.MakeVertex(x_max, y_max, z_max)
box = geompy.MakeBoxTwoPnt(box_min, box_max)
geompy.addToStudy(box, "selection_box")

# Get the nodes that are in the selection box
filter = smesh.GetFilter(SMESH.NODE,  SMESH.FT_BelongToGeom, box)
ids = mesh.GetIdsFromFilter(filter)

# Get the nodal coordinates
coords = []
for id in ids:
   coords.append(mesh.GetNodeXYZ(id))
   nbAdd = profile.Add([id])
  
# Save the coordinates (string)
file_id = open( r''+ExportPATH+'forma11c_profile.coords'+'', 'w')
for coord in coords:
    file_id.write(str(coord) + '\n')

file_id.close()

# Save the coordinates (json)
file_id = open( r''+ExportPATH+'forma11c_profile.json'+'', 'w')
json.dump(coords, file_id)
file_id.close()


# Update GUI tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
