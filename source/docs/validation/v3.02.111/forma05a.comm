# Start
DEBUT()

# Read the quadratic mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Change edge orientation 
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = _F(GROUP_MA = 'CD'))

# Barsoum
#mesh = MODI_MAILLAGE(reuse = mesh,
#                     MAILLAGE = mesh,
#                     MODI_MAILLE = _F(OPTION = 'NOEUD_QUART',
#                                      GROUP_NO_FOND = 'O'))

# Assign plane strain element (D_PLAN)
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'D_PLAN'))
                         
# Define material
elastic = DEFI_MATERIAU(ELAS = _F(E = 210000e6,
                                  NU = 0.3))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = elastic))

# Assign BCs
bcs = AFFE_CHAR_MECA(MODELE = model,
                     DDL_IMPO = (_F(GROUP_MA = 'OE',
                                    DY = 0.),
                                 _F(GROUP_NO = 'E',
                                    DX = 0.)),
                     FORCE_CONTOUR = _F(GROUP_MA = 'CD',
                                        FY = 10e6))

# Run static simulation
result = MECA_STATIQUE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = _F(CHARGE = bcs))

# Add stress components and invariants to the result
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELNO', 'SIEQ_NOEU'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Write output MED file
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result,
                    NOM_CHAM = ('SIGM_NOEU','SIEQ_NOEU','DEPL')))

#--------------------------------
# Post-process
#--------------------------------
# Define crack faces
crack = DEFI_FOND_FISS(MAILLAGE = mesh,
                       SYME = 'OUI',
                       FOND_FISS = _F(GROUP_NO = 'O'),
                       LEVRE_SUP = _F(GROUP_MA = 'GO'))

# Compute strain energy rate
r_max = 0.005 * 5
r_min = 0.005 * 2
G = CALC_G(OPTION = 'CALC_G',
           RESULTAT = result,
           THETA = _F(FOND_FISS = crack,
                      R_INF = r_min,
                      R_SUP = r_max))

GK = CALC_G(OPTION = 'CALC_K_G',
            RESULTAT = result,
            THETA = _F(FOND_FISS = crack,
                       R_INF = r_min,
                       R_SUP = r_max))

# Write table of strain energy values
IMPR_TABLE(TABLE = G)
IMPR_TABLE(TABLE = GK)


# Compute stress intensity factors
K = POST_K1_K2_K3(RESULTAT = result,
                  FOND_FISS = crack)

# Write table of stress intensity
IMPR_TABLE(TABLE = K)

#--------------------------------
# Verification
#--------------------------------
# G
G_ref = 192
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.511256986,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = G)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.511256986,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.527315126,
           VALE_REFE = 192,
           NOM_PARA = 'G_IRWIN',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 0.012,
           VALE_CALC = 193.008786982,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = K)

# K1
K1_ref = 6.65e6
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 6665536.76861,
           VALE_REFE = 6.650000E6,
           NOM_PARA = 'K1',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 5.0000000000000001E-3,
           VALE_CALC = 6673866.14366,
           VALE_REFE = 6.650000E6,
           NOM_PARA = 'K1',
           TABLE = K)

# Finish
FIN()
