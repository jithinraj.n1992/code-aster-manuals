# coding=utf-8
# Modal analysis of cantilevered plate
DEBUT(LANG = 'EN')

# Material parameters
E = 200.0e9
nu = 0.3
rho = 8000

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED',
                     UNITE = 20)

# Assign 3D elements
model = AFFE_MODELE(AFFE = _F(MODELISATION = ('3D', ),
                              PHENOMENE = 'MECANIQUE',
                              TOUT = 'OUI'),
                    MAILLAGE = mesh)

# Create and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu,
                                RHO = rho))

mater = AFFE_MATERIAU(AFFE = _F(MATER = steel,
                                TOUT = 'OUI'),
                      MODELE = model)

# Write out material and inertial properties to a table
mat_geom = POST_ELEM(MASS_INER = _F(TOUT = 'OUI'),
                     MODELE = model,
                     CHAM_MATER = mater)

IMPR_TABLE(TABLE = mat_geom,
           SEPARATEUR = ',')
 
# Displacement BCs
disp_bc = AFFE_CHAR_MECA(DDL_IMPO = _F(DX = 0.0,
                                       DY = 0.0,
                                       DZ = 0.0,
                                       GROUP_MA = ('fixed_face')),
                         MODELE = model)

# Assemble system
ASSEMBLAGE(CHAM_MATER = mater,
           CHARGE = disp_bc,
           MATR_ASSE = (_F(MATRICE = CO('stif_mat'),
                           OPTION = 'RIGI_MECA'),
                        _F(MATRICE = CO('mass_mat'),
                           OPTION = 'MASS_MECA')),
           MODELE = model,
           NUME_DDL = CO('num_dof'))

# Compute modes
result = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 50.0)),
                    MATR_MASS = mass_mat,
                    MATR_RIGI = stif_mat,
                    OPTION = 'BANDE')

# Compute normalized modes (using geneeralized mass)
result = NORM_MODE(reuse = result,
                   MODE = result,
                   NORME = 'MASS_GENE')

# Write results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)

# Verify results
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                    PARA = 'FREQ',
                    RESULTAT = result,
                    VALE_CALC = 6.28873),
                 _F(NUME_ORDRE = 2,
                    PARA = 'FREQ',
                    RESULTAT = result,
                    VALE_CALC = 15.352871942104),
                 _F(NUME_ORDRE = 3,
                    PARA = 'FREQ',
                    RESULTAT = result,
                    VALE_CALC = 38.5486),
                 _F(NUME_ORDRE = 4,
                    PARA = 'FREQ',
                    RESULTAT = result,
                    VALE_CALC = 49.0722)))

# Wrap up
FIN()
