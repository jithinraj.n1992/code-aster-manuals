import salome
import salome_notebook
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder

import GEOM
import SMESH

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()

# Create cube geometry
geompy = geomBuilder.New()
cube = geompy.MakeBoxDXDYDZ(1, 1, 1)
geompy.addToStudy(cube, 'box')

# Create geometry groups for vertices
node_1 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_2 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_3 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_4 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_5 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_6 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_7 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])
node_8 = geompy.CreateGroup(cube, geompy.ShapeType["VERTEX"])

# Create geometry groups for faces
front_face = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])
back_face  = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])
right_face = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])
left_face  = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])
top_face   = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])
bot_face   = geompy.CreateGroup(cube, geompy.ShapeType["FACE"])

# Create geometry group for volume
cube_vol = geompy.CreateGroup(cube, geompy.ShapeType["SOLID"])

# Locate and add vertices to the groups
point = geompy.MakeVertex(0, 1, 0)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_1, [vertex])
geompy.addToStudyInFather(cube, node_1, 'node_1')

point = geompy.MakeVertex(1, 1, 0)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_2, [vertex])
geompy.addToStudyInFather(cube, node_2, 'node_2')

point = geompy.MakeVertex(0, 0, 0)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_3, [vertex])
geompy.addToStudyInFather(cube, node_3, 'node_3')

point = geompy.MakeVertex(1, 0, 0)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_4, [vertex])
geompy.addToStudyInFather(cube, node_4, 'node_4')

point = geompy.MakeVertex(0, 1, 1)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_5, [vertex])
geompy.addToStudyInFather(cube, node_5, 'node_5')

point = geompy.MakeVertex(1, 1, 1)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_6, [vertex])
geompy.addToStudyInFather(cube, node_6, 'node_6')

point = geompy.MakeVertex(0, 0, 1)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_7, [vertex])
geompy.addToStudyInFather(cube, node_7, 'node_7')

point = geompy.MakeVertex(1, 0, 1)
vertex = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['VERTEX'])
geompy.UnionList(node_8, [vertex])
geompy.addToStudyInFather(cube, node_8, 'node_8')

# Locate and add faces to the groups
point = geompy.MakeVertex(0.5, 0.5, 1)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(front_face, [face])
geompy.addToStudyInFather(cube, front_face, 'front_face')

point = geompy.MakeVertex(0.5, 0.5, 0)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(back_face, [face])
geompy.addToStudyInFather(cube, back_face, 'back_face')

point = geompy.MakeVertex(1, 0.5, 0.5)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(right_face, [face])
geompy.addToStudyInFather(cube, right_face, 'right_face')

point = geompy.MakeVertex(0, 0.5, 0.5)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(left_face, [face])
geompy.addToStudyInFather(cube, left_face, 'left_face')

point = geompy.MakeVertex(0.5, 1, 0.5)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(top_face, [face])
geompy.addToStudyInFather(cube, top_face, 'top_face')

point = geompy.MakeVertex(0.5, 0, 0.5)
face = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['FACE'])
geompy.UnionList(bot_face, [face])
geompy.addToStudyInFather(cube, bot_face, 'bot_face')

# Add volume to the groups
point = geompy.MakeVertex(0.5, 0.5, 0.5)
vol = geompy.GetShapesNearPoint(cube, point, geompy.ShapeType['SOLID'])
geompy.UnionList(cube_vol, [vol])
geompy.addToStudyInFather(cube, cube_vol, 'cube')

# Start smesh
smesh = smeshBuilder.New()
cube_mesh = smesh.Mesh(cube)

# Set up algorithms
Regular_1D = cube_mesh.Segment()
Number_of_Segments_1 = Regular_1D.NumberOfSegments(1)
Quadrangle_2D = cube_mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_STANDARD,-1,[],[])
Hexa_3D = cube_mesh.Hexahedron(algo=smeshBuilder.Hexa)
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(Number_of_Segments_1, 'Number of Segments_1')

# Mesh the volume
isDone = cube_mesh.Compute()
smesh.SetName(cube_mesh, 'cube_mesh')

# From the geometry groups to the mesh
cube_1 = cube_mesh.GroupOnGeom(cube_vol, 'cube', SMESH.VOLUME)
front_1 = cube_mesh.GroupOnGeom(front_face, 'front_face', SMESH.FACE)
back_1 = cube_mesh.GroupOnGeom(back_face, 'back_face', SMESH.FACE)
right_1 = cube_mesh.GroupOnGeom(right_face, 'right_face', SMESH.FACE)
left_1 = cube_mesh.GroupOnGeom(left_face, 'left_face', SMESH.FACE)
top_1 = cube_mesh.GroupOnGeom(top_face, 'top_face', SMESH.FACE)
bottom_1 = cube_mesh.GroupOnGeom(bot_face, 'bot_face', SMESH.FACE)
node_1 = cube_mesh.GroupOnGeom(node_1, 'node_1', SMESH.NODE)
node_2 = cube_mesh.GroupOnGeom(node_2, 'node_2', SMESH.NODE)
node_3 = cube_mesh.GroupOnGeom(node_3, 'node_3', SMESH.NODE)
node_4 = cube_mesh.GroupOnGeom(node_4, 'node_4', SMESH.NODE)
node_5 = cube_mesh.GroupOnGeom(node_5, 'node_5', SMESH.NODE)
node_6 = cube_mesh.GroupOnGeom(node_6, 'node_6', SMESH.NODE)
node_7 = cube_mesh.GroupOnGeom(node_7, 'node_7', SMESH.NODE)
node_8 = cube_mesh.GroupOnGeom(node_8, 'node_8', SMESH.NODE)

# Export mesh
cube_mesh.ExportMED( r''+ExportPATH+'cube_mesh.mmed'+'')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
