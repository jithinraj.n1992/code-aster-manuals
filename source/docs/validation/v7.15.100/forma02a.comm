# Simulation of a pipe elbow mesh with hexahedral elements
DEBUT()

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Orient the interior surface where loads will be applied
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = _F(GROUP_MA='surf_interior'))

# Define material properties
steel = DEFI_MATERIAU(ELAS = _F(E = 204000000000.0,
                                NU = 0.3,
                                ALPHA = 1.096e-05),
                      THER = _F(LAMBDA = 54.6,
                                RHO_CP = 3710000.0))

#-----------------------------------------
# Transient thermal calculation:
#   Temperature prescribed on the interior
#   No heat flux on the exterior
#-----------------------------------------
# Create model
th_model = AFFE_MODELE(MAILLAGE = mesh,
                       AFFE = _F(TOUT = 'OUI',
                                 PHENOMENE = 'THERMIQUE',
                                 MODELISATION = '3D'))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = steel))

# Define temperature gradient function (function of time)
f_temp = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = ( 0, 20, 
                               10, 70))

# Apply temperature bc
th_bc = AFFE_CHAR_THER_F(MODELE = th_model,
                         TEMP_IMPO = _F(GROUP_MA = 'surf_interior',
                                        TEMP = f_temp))

# Create a list of time points to save
l_time = DEFI_LIST_REEL(VALE = (0, 5, 10))

# Define a constant function (function of time)
f_const = DEFI_FONCTION(NOM_PARA = 'INST',
                        VALE = ( 0, 1,
                                10, 1))

# Set up a linear thermal problem
th_resu = THER_LINEAIRE(MODELE = th_model,
                        CHAM_MATER = mater,
                        EXCIT = _F(CHARGE = th_bc,
                                   FONC_MULT = f_const),
                        INCREMENT = _F(LIST_INST = l_time),
                        ETAT_INIT = _F(VALE = 20))

#-----------------------------------------
# Thermo-mechanical calculation
#   Fixed at the pipe base and end face
#   Symmetry with respect to the XY plane
#   Thermal + pressure loads on interior
#-----------------------------------------
# Create model
meca_mod =AFFE_MODELE(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D'))

meca_mat = AFFE_MATERIAU(MAILLAGE = mesh,
                         MODELE = meca_mod,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = steel),
                         AFFE_VARC = _F(TOUT = 'OUI',
                                        NOM_VARC = 'TEMP',
                                        EVOL = th_resu,
                                        VALE_REF = 20))

meca_bc = AFFE_CHAR_MECA(MODELE = meca_mod,
                         DDL_IMPO = (_F(GROUP_MA = ('end_face', 'pipe_base'),
                                        LIAISON = 'ENCASTRE'),
                                     _F(GROUP_MA = 'symmetry',
                                        DZ = 0)),
                         PRES_REP = _F(GROUP_MA = 'surf_interior',
                                       PRES = 15000000.0));

result = MECA_STATIQUE(MODELE = meca_mod,
                       CHAM_MATER = meca_mat,
                       EXCIT = _F(CHARGE = meca_bc),
                       LIST_INST = l_time)

#-----------------------------------------
# Post-processing
#-----------------------------------------
# Add stress components and invariants to result
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELNO'),
                    CONTRAINTE = ('SIEF_ELNO'))

# Extract table of max values of stress
sig_max = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTREMA',
                                    INTITULE = 'sig_max',
                                    RESULTAT = result,
                                    NOM_CHAM = 'SIEQ_ELNO'))

# Extract table of stresses along a line through thickness
sig_line = MACR_LIGN_COUPE(RESULTAT = result,
                           NOM_CHAM = 'SIEF_ELNO',
                           LIGN_COUPE = _F(TYPE='SEGMENT',
                                           NB_POINTS = 10,
                                           COOR_ORIG = (0.18, 3, 0),
                                           COOR_EXTR = (0.20, 3, 0)))

# Save the results
IMPR_TABLE(TABLE = sig_line,
           NOM_PARA ='SIYY')

IMPR_RESU(FORMAT = 'MED',
          RESU = (_F(MAILLAGE = mesh,
                     RESULTAT = result),
                  _F(RESULTAT = th_resu)))

IMPR_RESU(FORMAT = 'IDEAS',
          VERSION = 5,
          RESU = (_F(MAILLAGE = mesh,
                     RESULTAT = result),
                  _F(RESULTAT = th_resu)))

# Regression test
TEST_RESU(RESU = (_F(GROUP_NO = 'A_i1',
                     INST = 10,
                     RESULTAT = th_resu,
                     NOM_CHAM = 'TEMP',
                     NOM_CMP = 'TEMP',
                     VALE_CALC = 70),
                  _F(GROUP_NO = 'node_mid',
                     INST = 0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = -4.51915834812E-4),
                  _F(INST = 10,
                     TYPE_TEST = 'MAX',
                     RESULTAT = result,
                     NOM_CHAM ='SIEQ_ELNO',
                     NOM_CMP ='VMIS',
                     VALE_CALC = 194395274.29)))

# Finish
FIN();
