#!/usr/bin/env python
import sys
import salome
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import GEOM
import SMESH
import math

ExportPATH="/home/banerjee/Salome/forma12/"

salome.salome_init()
geompy = geomBuilder.New()
smesh = smeshBuilder.New()

# Geometry parameters
length = 2.0
width = 2.0
thickness = 0.03

# Mesh parameters
num_thru_thick = 2
num_along_edge = 20

#-------------
# Geometry
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Create plate
plate = geompy.MakeBoxDXDYDZ(length, width, thickness)
plate_id = geompy.addToStudy(plate, "plate")

# Create groups
pt_O = geompy.MakeVertex(0, 0, 0)
vert_O = geompy.GetVertexNearPoint(plate, pt_O)
geompy.addToStudyInFather(plate, vert_O, "point_O")

pt_P = geompy.MakeVertex(length, width, thickness)
vert_P = geompy.GetVertexNearPoint(plate, pt_P)
geompy.addToStudyInFather(plate, vert_P, "point_P")

pt_m = geompy.MakeVertex(0, 0, thickness/2.0)
edge_L = geompy.GetEdgeNearPoint(plate, pt_m)
geompy.addToStudyInFather(plate, edge_L, "edge_L")

pt_fixed = geompy.MakeVertex(length/2, 0, thickness/2.0)
fixed_face = geompy.GetFaceNearPoint(plate, pt_fixed)
geompy.addToStudyInFather(plate, fixed_face, "fixed_face")

#-------------
# Mesh
plate_mesh = smesh.Mesh(plate)

# Set hypothesis
Regular_1D = plate_mesh.Segment()
Regular_1D_1 = plate_mesh.Segment(geom = edge_L)
Quadrangle_2D = plate_mesh.Quadrangle(algo = smeshBuilder.QUADRANGLE)
Hexa_3D = plate_mesh.Hexahedron(algo = smeshBuilder.Hexa)

# Set parameters
Number_of_Segments = Regular_1D.NumberOfSegments(20)
Number_of_Segments_1 = Regular_1D_1.NumberOfSegments(2)
Propagation_of_1D_Hyp = Regular_1D_1.Propagation()

# Compute mesh
isDone = plate_mesh.Compute()

# Convert to quadratic
plate_mesh.ConvertToQuadratic(0)

# Move geometry groups to mesh
g_fixed_face = plate_mesh.GroupOnGeom(fixed_face, 'fixed_face', SMESH.FACE)
g_edge_L = plate_mesh.GroupOnGeom(edge_L, 'edge_L', SMESH.EDGE)
g_pt_O = plate_mesh.GroupOnGeom(vert_O, 'point_O', SMESH.NODE)
g_pt_P = plate_mesh.GroupOnGeom(vert_P, 'point_P', SMESH.NODE)

# Add group for whole volume
plate_vol = plate_mesh.CreateEmptyGroup( SMESH.VOLUME, 'plate_vol' )
nbAdd = plate_vol.AddFrom( plate_mesh.GetMesh() )

# Export mesh
plate_mesh.ExportMED( r''+ExportPATH+'forma12a_submesh.mmed'+'')

# Update GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
