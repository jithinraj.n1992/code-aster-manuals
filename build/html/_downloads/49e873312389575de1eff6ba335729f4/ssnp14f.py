# Cube composed of six linear tetrahedron elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node2 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node3 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node4 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node5 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node6 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node7 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node8 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 1.00000000E+00)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_1')
node_2 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_2')
node_3 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_3')
node_4 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_4')
node_5 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_5')
node_6 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_6')
node_7 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_7')
node_8 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_8')
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])

# Add volume elements
vol1 = mesh.AddVolume([1, 7, 6, 5])
vol2 = mesh.AddVolume([2, 1, 6, 7])
vol3 = mesh.AddVolume([3, 2, 1, 7])
vol4 = mesh.AddVolume([3, 4, 2, 7])
vol5 = mesh.AddVolume([7, 8, 4, 6])
vol6 = mesh.AddVolume([2, 6, 7, 4])

# Add group for the volumes
cube = mesh.CreateEmptyGroup(SMESH.VOLUME, 'cube')
tet1 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet1')
tet2 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet2')
tet3 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet3')
tet4 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet4')
tet5 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet5')
tet6 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet6')
nbAdd = cube.Add([1, 2, 3, 4, 5, 6])
nbAdd = tet1.Add([1])
nbAdd = tet2.Add([2])
nbAdd = tet3.Add([3])
nbAdd = tet4.Add([4])
nbAdd = tet5.Add([5])
nbAdd = tet6.Add([6])

# Add face elements
face7 = mesh.AddFace([4, 2, 6])
face8 = mesh.AddFace([4, 6, 8])
face9 = mesh.AddFace([2, 1, 6])
face10 = mesh.AddFace([1, 5, 6])
face11 = mesh.AddFace([5, 7, 1])
face12 = mesh.AddFace([7, 3, 1])
face13 = mesh.AddFace([3, 4, 7])
face14 = mesh.AddFace([4, 8, 7])

# Add groups for the faces
left_face = mesh.CreateEmptyGroup( SMESH.FACE, 'left_face' )
right_face = mesh.CreateEmptyGroup( SMESH.FACE, 'right_face' )
top_face = mesh.CreateEmptyGroup( SMESH.FACE, 'top_face' )
bot_face = mesh.CreateEmptyGroup( SMESH.FACE, 'bot_face' )
nbAdd = left_face.Add([11, 12])
nbAdd = right_face.Add([7, 8])
nbAdd = top_face.Add([9, 10])
nbAdd = bot_face.Add([13, 14])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp14f.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
