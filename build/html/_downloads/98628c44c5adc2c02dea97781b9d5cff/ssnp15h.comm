# 2d plane stress square element in combined tension + shear
# linear in model B converted into quadratic
# reduced integration
# J2 plasticity + isotropic hardening
DEBUT(LANG='EN')

# Read the mesh
mesh_in = LIRE_MAILLAGE(FORMAT = "MED")

# Convert to quadratic
mesh = CREA_MAILLAGE(MAILLAGE = mesh_in,
                     LINE_QUAD = _F(TOUT = 'OUI'))

# Create a group for the new node NS2
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(TOUT_GROUP_MA = 'OUI'))

mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(NOM = 'node_2_NS2',
                                      DIFFE = ('right_edge', 'node_4'))))

# Assign plane stress mechanical element
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'C_PLAN_SI'))

# Material parameters
E = 195000.0
nu = 0.3
h_lin_iso = 1930.0
sig_y = 181.0

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu),
                      ECRO_LINE = _F(D_SIGM_EPSI = h_lin_iso,
                                     SY = sig_y),
                      PRAGER = _F(C = 0))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                 MATER = steel))

# Define traction functions
# Tension
sigma_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0.0, 0.0,
                                 1.0, 151.2,
                                 2.0, 257.2,
                                 3.0, 0.0),
                         PROL_DROITE = 'EXCLU',
                         PROL_GAUCHE = 'EXCLU')

# Shear
tau_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               1.0, 93.1,
                               2.0, 33.1,
                               3.0, 0.0),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')

# Apply a unit traction BC for tension in -x
tens_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_CONTOUR = _F(GROUP_MA = 'left_edge',
                                            FX = -1.))

# Apply unit traction bcs for shear in xy-plane
shear_bc = AFFE_CHAR_MECA(MODELE = model,
                          FORCE_CONTOUR = (_F(GROUP_MA = 'left_edge',
                                              FY = -1.),
                                           _F(GROUP_MA = 'right_edge',
                                              FY = 1.),
                                           _F(GROUP_MA = 'top_edge',
                                              FX = 1.),
                                           _F(GROUP_MA = 'bot_edge',
                                              FX = -1.)))

# Apply displacement BCs to prevent rigid body motions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_NO = 'node_4',
                                        DX = 0.,
                                        DY = 0.),
                                     _F(GROUP_NO = ('node_2_NS2'),
                                        DX = 0.)))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 0.1,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 0.9,
                                          NOMBRE = 10),
                                       _F(JUSQU_A = 1.0,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = 40),
                                       _F(JUSQU_A = 3.0,
                                          NOMBRE = 1)))


# Run simulation (1)
# VMIS_ISOT_LINE
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = tens_bc,
                                   FONC_MULT = sigma_fn),
                                _F(CHARGE = shear_bc,
                                   FONC_MULT = tau_fn)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE'),
                       INCREMENT = _F(LIST_INST = l_times),
                       NEWTON = _F(MATRICE = 'TANGENTE',
                                   REAC_ITER = 3),
                       RECH_LINEAIRE = _F(RHO_MAX = 150),
                       CONVERGENCE = _F(ITER_GLOB_MAXI = 50))

# Run simulation (2)
# VMIS_ECMI_LINE
result2 = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = disp_bc),
                                 _F(CHARGE = tens_bc,
                                    FONC_MULT = sigma_fn),
                                 _F(CHARGE = shear_bc,
                                    FONC_MULT = tau_fn)),
                        COMPORTEMENT = _F(RELATION = 'VMIS_ECMI_LINE'),
                        INCREMENT = _F(LIST_INST = l_times),
                        NEWTON = _F(MATRICE = 'TANGENTE',
                                    REAC_ITER = 3),
                        RECH_LINEAIRE = _F(RHO_MAX = 150),
                        CONVERGENCE = _F(ITER_GLOB_MAXI = 50))

# Calculate additional data (1)
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_ELGA'),
                    ENERGIE = ('ETOT_ELGA', 'ETOT_ELNO', 'ETOT_ELEM'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    DEFORMATION = ('EPSI_ELNO', 'EPSI_ELGA', 'EPSP_ELGA'))

# Calculate additional data (2)
result2 = CALC_CHAMP(reuse = result2,
                     RESULTAT = result2,
                     CRITERES = ('SIEQ_ELGA'),
                     CONTRAINTE = ('SIGM_ELNO', 'SIGM_ELGA'),
                     ENERGIE = ('ETOT_ELGA', 'ETOT_ELNO', 'ETOT_ELEM'),
                     VARI_INTERNE = ('VARI_ELNO'),
                     DEFORMATION = ('EPSI_ELNO', 'EPSI_ELGA', 'EPSP_ELGA'))

# Extract internal variables (1)
intvar = CREA_CHAMP(TYPE_CHAM = 'ELNO_VARI_R',
                    OPERATION = 'EXTR',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_ELNO',
                    INST = 1.)

# Extract internal variables (2)
intvar2 = CREA_CHAMP(TYPE_CHAM = 'ELNO_VARI_R',
                     OPERATION = 'EXTR',
                     RESULTAT = result2,
                     NOM_CHAM = 'VARI_ELNO',
                     INST = 1.)

# Compute total strain energy (1)
tot_en = POST_ELEM(ENER_TOTALE = _F(GROUP_MA = ('cube')),
                   RESULTAT = result)
  
# Compute total strain energy (2)
tot_en2 = POST_ELEM(ENER_TOTALE = _F(GROUP_MA = ('cube')),
                    RESULTAT = result2)

tot_en21 = POST_ELEM(ENER_TOTALE = _F(TOUT = 'OUI'),
                     RESULTAT = result2)

# Compute indicators of loss of proportionality in loading (1)
ind_en = POST_ELEM(INDIC_ENER = _F(TOUT = 'OUI'),
                   RESULTAT = result,
                   INST = (1.0, 2.0, 3.0))

ind_dir = POST_ELEM(INDIC_SEUIL = _F(TOUT = 'OUI'),
                    RESULTAT = result,
                    NUME_ORDRE = (12, 52, 53))

# Write results for visualization
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result2),
          UNITE = 81)

# Extract results for stress-strain plots (xx and xy)
sig_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result)
eps_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result)
sig_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result)
eps_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result)

sig_xx_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result2)
eps_xx_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result2)
sig_xy_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result2)
eps_xy_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result2)

# Write results for plotting
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_1),
                        _F(FONCTION = eps_xx_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_1),
                        _F(FONCTION = eps_xy_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_2),
                        _F(FONCTION = eps_xx_2)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_2),
                        _F(FONCTION = eps_xy_2)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)

#----------------------------------------
# Verification tests
#----------------------------------------
TEST_RESU(CHAM_ELEM = _F(CHAM_GD = intvar,
                         GROUP_MA = 'cube',
                         GROUP_NO = 'node_2',
                         NOM_CMP = 'V1',
                         VALE_CALC = 0.020547265463459))

TEST_RESU(RESU = (_F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOM_CMP = 'SIXX',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 151.19999999657),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOM_CMP = 'SIXY',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 93.1),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'EPSI_ELNO',
                     NOM_CMP = 'EPXX',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 0.014829713606921),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'EPSI_ELNO',
                     NOM_CMP = 'EPXY',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 0.013601401082680),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'VARI_ELNO',
                     NOM_CMP = 'V1',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 0.020547265463459),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'VARI_ELNO',
                     NOM_CMP = 'V2',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_CALC = 1.0),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'EPSI_ELNO',
                     NOM_CMP = 'EPXX',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_REFE = 0.035265,
                     PRECISION = 0.01,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.035324715),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'EPSI_ELNO',
                     NOM_CMP = 'EPXY',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_REFE = 0.020471,
                     PRECISION = 0.01,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.020351413),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXX',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     VALE_REFE = 0.033946,
                     PRECISION = 0.01,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.03400574),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXY',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     VALE_REFE = 0.02025,
                     PRECISION = 0.01,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.020130747),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'VARI_ELNO',
                     NOM_CMP = 'V1',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     VALE_REFE = 0.042329,
                     PRECISION = 0.01,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.042329286),
                  _F(RESULTAT = result,
                     INST = 1.0,
                     NOM_CHAM = 'SIEQ_ELGA',
                     NOM_CMP = 'TRIAX',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     VALE_REFE = 0.228,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.22799999),
                  _F(RESULTAT = result,
                     INST = 2.0,
                     NOM_CHAM = 'SIEQ_ELGA',
                     NOM_CMP = 'TRIAX',
                     GROUP_MA = 'cube',
                     POINT = 4,
                     VALE_REFE = 0.325349,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.325348653)))
  
TEST_RESU(RESU = (_F(RESULTAT = result2,
                     INST = 0.1,
                     NOM_CHAM = 'ETOT_ELGA',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 0.00116403,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 0.00116403144),
                  _F(RESULTAT = result2,
                     INST = 0.9,
                     NOM_CHAM = 'ETOT_ELGA',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 1.8434,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 1.84222238),
                  _F(RESULTAT = result2,
                     INST = 2.0,
                     NOM_CHAM = 'ETOT_ELGA',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.58487,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.583429296),
                  _F(RESULTAT = result2,
                     INST = 3.0,
                     NOM_CHAM = 'ETOT_ELGA',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.40794,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.406505127),
                  _F(RESULTAT = result2,
                     INST = 0.1,
                     NOM_CHAM = 'ETOT_ELNO',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     CRITERE = 'RELATIF',
                     VALE_REFE = 0.00116403,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 0.00116403144),
                  _F(RESULTAT = result2,
                     INST = 0.9,
                     NOM_CHAM = 'ETOT_ELNO',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     CRITERE = 'RELATIF',
                     VALE_REFE = 1.8434,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 1.84222238),
                  _F(RESULTAT = result2,
                     INST = 2.0,
                     NOM_CHAM = 'ETOT_ELNO',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.58487,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.583429296),
                  _F(RESULTAT = result2,
                     INST = 3.0,
                     NOM_CHAM = 'ETOT_ELNO',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     GROUP_NO = 'node_2',
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.40794,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.406505127),
                  _F(RESULTAT = result2,
                     INST = 0.1,
                     NOM_CHAM = 'ETOT_ELEM',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 0.00116403,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 0.00116403144),
                  _F(RESULTAT = result2,
                     INST = 0.9,
                     NOM_CHAM = 'ETOT_ELEM',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 1.8434,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 1.84222238),
                  _F(RESULTAT = result2,
                     INST = 2.0,
                     NOM_CHAM = 'ETOT_ELEM',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.58487,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.583429296),
                  _F(RESULTAT = result2,
                     INST = 3.0,
                     NOM_CHAM = 'ETOT_ELEM',
                     NOM_CMP = 'TOTALE',
                     GROUP_MA = 'cube',
                     POINT = 1,
                     CRITERE = 'RELATIF',
                     VALE_REFE = 9.40794,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_CALC = 9.406505127)))
  
TEST_TABLE(TABLE = tot_en2,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.1),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 0.00116403,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 0.00116403144)
  
TEST_TABLE(TABLE = tot_en2,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.9),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 1.8434,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 1.84222238)
  
TEST_TABLE(TABLE = tot_en2,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 2.0),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 9.58487,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 9.583429296)
  
TEST_TABLE(TABLE = tot_en2,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 3.0),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 9.40794,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 9.406505127)
  
  
TEST_TABLE(TABLE = tot_en21,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.1),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 0.00116403,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 0.00116403144)
  
TEST_TABLE(TABLE = tot_en21,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.9),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 1.8434,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 1.84222238)
  
TEST_TABLE(TABLE = tot_en21,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 2.0),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 9.58487,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 9.583429296)
  
TEST_TABLE(TABLE = tot_en21,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 3.0),
           NOM_PARA = 'TOTALE',
           CRITERE = 'RELATIF',
           VALE_REFE = 9.40794,
           REFERENCE = 'AUTRE_ASTER',
           VALE_CALC = 9.406505127)
  
  
TEST_TABLE(TABLE = ind_en,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 12),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_ENER',
           CRITERE = 'ABSOLU',
           VALE_REFE = 0.0,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.0)
  
TEST_TABLE(TABLE = ind_en,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 52),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_ENER',
           VALE_REFE = 0.0326,
           PRECISION = 0.03,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.033376265)
  
TEST_TABLE(TABLE = ind_en,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 53),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_ENER',
           VALE_REFE = 0.0469,
           PRECISION = 0.03,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.047729905)
  
TEST_TABLE(TABLE = ind_dir,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 12),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_SEUIL',
           CRITERE = 'ABSOLU',
           VALE_REFE = 0.0,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.0)
  
TEST_TABLE(TABLE = ind_dir,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 52),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_SEUIL',
           VALE_REFE = 0.0971,
           PRECISION = 0.01,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.096406983)
  
TEST_TABLE(TABLE = ind_dir,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 53),
                     _F(NOM_PARA = 'LIEU',
                        VALE_K = 'mesh')),
           NOM_PARA = 'INDIC_SEUIL',
           VALE_REFE = 1.0,
           PRECISION = 0.01,
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 1.0)
  
# End
FIN()
