.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v6.04.165:

*********************************************************************************************
**V6.04.165**: FORMA42 - Prestressed concrete beams with variable section under gravity load
*********************************************************************************************

The purpose of this tutorial is to examine two cable tensioning techniques (prestressing) in ``Code_Aster``
and compare the solutions.

* :ref:`v6.04.165.2`:

  In model A, all the cables are set in tension simultaneously from the beginning of the computation
  by using the operator :red:`STAT_NON_LINE`

* :ref:`v6.04.165.3`:

  In model B we use the operator :red:`CALC_PRECONT`. All the cables are tensioned simultaneously.

* :ref:`v6.04.165.4`:

  Model C also uses the operator :red:`CALC_PRECONT`.  However, in this case, the phased prestressing is modeled.

In all the cases, the connections between the cable and the concrete as well as the calculation of the tensions
according to ``BPEL91`` are modeled with the operator :red:`DEFI_CABLE_BP`.

When similar simulations are performed in limited time, it can be judicious to start with Model A, and to
the use the approaches in models B and C if time permits.

.. _v6.04.165.1:

Reference problem
==================

.. _v6.04.165.1.1:

Geometry
--------

.. image:: v6.04.165/fig2_v6.04.165.png
   :height: 300 px
   :align: right

We consider a reinforced concrete beam of square section, composed of two segments 10 meters in length each.
The first segment has a cross-section of 1 m :math:`^2` while the second segment has a cross-section
of 4 m :math:`^2`. The beam is vertical with the weakest section at the bottom. It is embedded at its base, and
contains 5 straight cables for prestressing. The five cables run through the entire length of the beam
and are located as shown in the figure below.  The cross-sectional area of each cable is 25 cm :math:`^2`.

.. _fig_v6.04.165.1:

.. figure:: v6.04.165/fig1_v6.04.165.png
   :height: 150 px
   :align: center

   Cross section of beam with cable locations

.. _v6.04.165.1.2:

Material properties
-------------------

Concrete material constituting the beam:

* Young's modulus: :math:`E_c` = 41 GPa
* Poisson's ratio: :math:`\nu` = 0.2
* Mass density : :math:`\rho` = 2500 kg / m :math:`^3`

Steel material constituting the cable:

* Young's modulus: :math:`E_s` = 193 GPa
* Poisson's ratio: :math:`\nu` = 0.3
* Mass density: "math:`\rho` = 7850 kg / m :math:`^3`)

Characteristics concerning the tensioning of the cables:

* anchor setback: 1 mm
* linear friction coefficient: 0.0015 m :math:`^{-1}`
* tensile force at the ends of a cable: 3750 kN
* time before forming structure is stripped: 150 days
* first cable tensioning age: 300 days

.. _v6.04.165.1.3:

Boundary conditions and loads
-----------------------------

The base of the beam is fixed in the Z direction. The two translational motions with respect to OX and OY are
prevented, as well as rotation around OZ.

The loading sequence is as follows:

* at 300 days, tensioning of 2 cables (1 and 2) at their lower end,
* at 450 days, tensioning of 2 additional cables (3 and 4) always at their lower end,
* at 600 days, tensioning of the last cable (5) at its two ends.

The beam is also subjected to gravity.

.. _v6.04.165.2:

**Model A**: Tensioning of the beam with :red:`STAT_NON_LINE` without phased loading
=====================================================================================

.. _v6.04.165.2.1:

Creating and running the tutorial
---------------------------------

.. _v6.04.165.2.1.1:

Creating the geometry and the mesh
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A mesh of the geometry is provided in the ``Code_Aster`` installation in association with the validation
problem :ref:`SSNV164 - [V6.04.164] <v6.04.164>`.  However, we will create a new mesh of the model with
``Salome-Meca``.  The Python script for the model and mesh can be downloaded from
:download:`ssnv164a.py <v6.04.165/ssnv164a.py>`.  The file is listed below for convenience.

.. include::  v6.04.165/ssnv164a.py
     :literal:

This Python script produces a MED file named ``ssnv16a_new.mmed``.

.. _v6.04.165.2.1.2:

Creating the command file
^^^^^^^^^^^^^^^^^^^^^^^^^

In this model one does not model the time-dependent loading; all the loads are applied in a single timestep.

* Import the mesh in MED format (``ssnv164a_new.mmed``) and visualize it
* The steps needed to create a simulation stage in ``AsterStudy`` are listed below:

  * Read the mesh: :red:`LIRE_MAILLAGE`
  * Create nodes groups starting from element groups: :red:`DEFI_GROUP` on the element groups
    :green:`bot_face` and the cable elements :green:`cable0`, :green:`cable1`, :green:`cable2`,
    :green:`cable3`, and :green:`cable4`.  Boundary conditions are applied on the bottom face.  The
    axis of symmetry is also fixed.  However, a separate node group, :green:`center_nodes`, is created for
    this axis during the meshing stage.
  * Define the model: :red:`AFFE_MODELE`: assign mechanical :blue:`3D` modeling for the volume element
    group :green:`vol_all` (the complete beam) and mechanical type :blue:`BARRE` for the cable element groups
    (:green:`cable0`, :green:`cable1`, etc.)
  * Define section information for the cables: :red:`AFFE_CARA_ELEM`: the radius of the cables is
    :math:`R` = 2.8209 cm , keyword :blue:`BARRE`.
  * Create the concrete material: :red:`DEFI_MATERIAU`:  (:math:`E` = 41 GPa, :math:`\nu` = 0.2,
    :math:`\rho` = 2500 kg / m :math:`^3`) with keyword :blue:`ELAS` and :blue:`BPEL_BETON`
    (creep / recovery by default).
  * Create the cable material (:math:`E` = 193 GPa, :math:`\nu` = 0.3, :math:`\rho` = 7850 kg / m :math:`^3`).
    Use an elastic law and the regulatory prestressing specifications (:blue:`BPEL_ACIER` keyword with
    :blue:`F_PRG` = 1940.E6, :blue:`FROT_COURB` = 0, :blue:`FROT_LINE` = 1.5e-3).
  * Assign the materials: :red:`AFFE_MATERIAU`: concrete material to :green:`vol_all` and steel material to
    the cables.
  * Define the boundary conditions (loads, displacements): :red:`AFFE_CHAR_MECA`:

    * :blue:`DX = 0` on the node group :green:`py`
    * :blue:`DY = 0` on the node group :green:`px`
    * :blue:`DX, DY = 0` on the node group :green:`center_nodes`
    * :blue:`DZ = 0` on the node group :green:`bot_face`
    * assign gravity loading (keyword :blue:`PESANTEUR`).

  * Define the prestressing of the cables: :red:`DEFI_CABLE_BP`. For all cables, use an initial tension of
    3751 kN and an anchoring setback of 0.001 m; the active anchor is the one at the narrow end of the beam
    (e.g., :green:`pbot0` for the first cable), the passive at the wide end (e.g., :green:`ptop0` for the
    first cable).
  * Use :red:`AFFE_CHAR_MECA` to define the relation between the cables and the concrete. Be careful to
    explicitly specify activation of the kinematic relation between cables and concrete
    (:blue:`RELA_CINE = 'OUI'`) and the load related to prestressing (:blue:`SIGM_BPEL = 'OUI'`).
  * Define the list of times that will be used to solve the mechanical problem using
    :red:`DEFI_LIST_REEL`.
  * Define :red:`STAT_NON_LINE` for mechanical computation. Concrete and cables are treated as elastic bands.
  * Calculate with :red:`CALC_CHAMP`, the variables which you wish to post-process with ``Salomé`` (at least
    the stresses) and to print them in MED format.)
  * Recover the forces that will be applied in cable 0 using the command :red:`RECU_TABLE`. Print them with
    :red:`IMPR_TABLE`.
  * Recover the forces in the various cables after tensioning the cables using :red:`POST_RELEVE_T`. Print them
    with :red:`IMPR_TABLE`.
  * Compare the curves by plotting the tension force curves as a function of the curvilinear abscissa:

    1. that defined in operator :red:`DEFI_CABLE_BP`
    2. at the initial timestep
    3. at the final timestep of the results resulting from :red:`STAT_NON_LINE`.

    Use the operator :red:`RECU_FONCTION` to extract the curve data and then
    :red:`IMPR_FONCTION (FORMAT = 'TABLEAU')` to save the data in CSV files that can be plotted in Python.

The command file can be downloaded from :download:`forma42a.comm <v6.04.165/forma42a.comm>`.  The file is
listed below for convenience.

.. include::  v6.04.165/forma42a.comm
     :literal:

.. _v6.04.165.2.1.3:

Additional exploration
^^^^^^^^^^^^^^^^^^^^^^

Vary the input parameters on the cable tension calculation: anchor recoil, active / passive anchors, and
relaxation loss. Compare the tensions in the cables.

.. _v6.04.165.2.2:

Results and verification tests
------------------------------

The displacements and stresses in the concrete can be visualized in the :blue:`Results` tab of ``AsterStudy``.
Plots at :math:`t` = 600 are shown below.

.. list-table::

   * - .. figure:: v6.04.165/forma42a_disp_600.png

          Displacement

     - .. figure:: v6.04.165/forma42a_sigz_600.png

          Stress in the vertical direction

Plots of cable tension as a function of height extracted from the tables saved with :red:`IMPR_TABLE` are shown
below. The Jupyter Notebook used to plot the figures below can be found at
:download:`plot_forma42.ipynb <v6.04.165/plot_forma42.ipynb>`

.. figure:: v6.04.165/forma42a_8.svg
   :height: 350 px
   :align: center

   Plot of data extracted with :red:`IMPR_RESU`

.. warning::
    
   The data extracted with :red:`RECU_FONCTION` and saved with :red:`IMPR_FONCTION` appears to be
   incorrect (possibly due to a bug).  A plot of the data is shown below.

   .. figure:: v6.04.165/forma42a_38.svg
      :height: 350 px
      :align: center

      Plot of data extracted with :red:`IMPR_FONCTION`

Reference values via a CASTEM calculation at :math:`t` = 600 days are listed below for verification purposes.
The node and element numbers correspond to node groups at increasing heights along a cable (see the Python
mesh generation script for the coordinates).

+--------------------------------+-----------------------+---------------+
| Identification(node - element) | Reference value (kN ) | Tolerance (%) |
+================================+=======================+===============+
| N1 - M5655                     | 3519                  | 3.00          |
+--------------------------------+-----------------------+---------------+
| N6 - M5660                     | 3546                  | 3.00          |
+--------------------------------+-----------------------+---------------+
| N11 - M5664                    | 3597                  | 3.50          |
+--------------------------------+-----------------------+---------------+
| N16 - M5670                    | 3635                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N101 - M5674                   | 3614                  | 6.00          |
+--------------------------------+-----------------------+---------------+

.. _v6.04.165.3:

**Model B**: Tensioning of the beam with :red:`CALC_PRECONT` without phased loading
====================================================================================

.. _v6.04.165.3.1:

Creating and running the tutorial
---------------------------------

.. _v6.04.165.3.1.1:

Preliminary step if the **Model A** was not explored
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Import the ``form042a.comm`` file in ``AsterStudy``.
* Analyze the command file to understand the different stages.

Some post-processing for cable 0 is required in order to control:

* the tension calculated with :red:`DEFI_CABLE_BP` according to the BPEL node :green:`N1` : ``n1-i``
* the tension which is applied, ``n1_0``
* the tension after calculation of the equilibrium at :math:`t` = 600 days: ``n1_600``.

* Fill in the necessary output files then launch the calculation.

In the rest of the tutorial, the differences in tension profile obtained according to the tensioning mode will
be of interest.

.. _v6.04.165.3.1.2:

Change from :red:`STAT_NON_LINE` to :red:`CALC_PRECONT`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Model B is identical to Model A, except that tensioning of the cables is conducted using operator
:red:`CALC_PRECONT` instead of :red:`STAT_NON_LINE`.  The changes to be made are as follows:

* Modify :red:`AFFE_CHAR_MECA` so as to no longer seek to apply the prestressing :blue:`(SIGM_BPEL = 'NON')`
* Change :red:`STAT_NON_LINE` into :red:`CALC_PRECONT`. Remove cable prestress loading and inform the keyword
  :blue:`CABLE_BP`.

Compare the tension profiles of Model A and Model B.

.. _v6.04.165.3.2:

Results and verification tests
-------------------------------

The displacements and stresses in the concrete can be visualized in the :blue:`Results` tab of ``AsterStudy``.
Plots at :math:`t` = 600 are shown below.

.. list-table::

   * - .. figure:: v6.04.165/forma42b_disp_600.png

          Displacement

     - .. figure:: v6.04.165/forma42b_sigz_600.png

          Stress in the vertical direction

Plots of cable tension as a function of height extracted from the tables saved with :red:`IMPR_TABLE` are shown
below. The values at :math:`t` = 0 are identically zero and do not show
up in the plot.
The Jupyter Notebook used to plot the figures below can be found at
:download:`plot_forma42.ipynb <v6.04.165/plot_forma42.ipynb>`

.. figure:: v6.04.165/forma42b_8.svg
   :height: 350 px
   :align: center

   Plot of data extracted with :red:`IMPR_RESU`

.. warning::
    
   The data extracted with :red:`RECU_FONCTION` and saved with :red:`IMPR_FONCTION` appears to be
   incorrect (possibly due to a bug).  A plot of the data is shown below.

   .. figure:: v6.04.165/forma42b_38.svg
      :height: 350 px
      :align: center

      Plot of data extracted with :red:`IMPR_FONCTION`

The reference values are identical to those for Model A, but tolerances are different for Model B.

+--------------------------------+-----------------------+---------------+
| Identification(node - element) | Reference value (kN ) | Tolerance (%) |
+================================+=======================+===============+
| N1 - M5655                     | 3519                  | 4.00          |
+--------------------------------+-----------------------+---------------+
| N6 - M5660                     | 3546                  | 4.00          |
+--------------------------------+-----------------------+---------------+
| N11 - M5664                    | 3597                  | 3.00          |
+--------------------------------+-----------------------+---------------+
| N16 - M5670                    | 3635                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N101 - M5674                   | 3614                  | 1.00          |
+--------------------------------+-----------------------+---------------+

.. _v6.04.165.4:

**Model C**: Phased Tensioning of the beam with multiple :red:`CALC_PRECONT` calls
====================================================================================

.. _v6.04.165.4.1:

Creating and running the tutorial
---------------------------------

In this case, we can resume the study in Model B and add phased prestressing.  The steps are
as follows:

* At the beginning, the cables are not yet in place (there is only the sheath), on the other hand there is
  the effect of gravity on concrete.
* The cables :green:`cable0` and :green:`cable1` are put under tension at 300 days (the other cables being inactive).
* The cables :green:`cable2` and :green:`cable3` are put under tension at 450 days.
* At 600 days, we tension the cable :green:`cable4`.

Modify the command file accordingly and compare the tension profiles in the cables compared to previous cases.

Actions / commands to use:

* Duplicate the :red:`DEFI_CABLE_BP` to stretch the 3 separate cable groups.
* Do the same for the loads associated with these cables. These loads must include only the kinematic connections.
* Modify of the list of times for the computation of prestressing: :red:`DEFI_LIST_REEL`,
  values ​​(0,150,300,450,600) days.
* Make a first :red:`STAT_NON_LINE` for the application of the gravity load (1 time step).

  .. warning::

     Do not take into account the cables. It is advisable to assign them the material behavior :blue:`SANS`,
     which amounts to applying a zero Young's modulus.

* Then add 3 calls to :red:`CALC_PRECONT` for the successive tensioning of the cables.
* The boundary conditions change in all 3 cases.

* Use the same postprocessing as before but save the curves for different time spans or at least at t = 600 days.

Observe the differences in the tensions in the cables and in the state of stress in the concrete.

.. _v6.04.165.4.2:

Results and verification tests
-------------------------------

The displacements and stresses in the concrete can be visualized in the :blue:`Results` tab of ``AsterStudy``.
Plots at :math:`t` = 600 are shown below.

.. list-table::

   * - .. figure:: v6.04.165/forma42c_disp_600.png

          Displacement

     - .. figure:: v6.04.165/forma42c_sigz_600.png

          Stress in the vertical direction

Plots of cable tension as a function of height extracted from the tables saved with :red:`IMPR_TABLE` are shown
below. The values at :math:`t` = 0  and :math:`t` = 150 are identically zero and do not show
up in the plot.
The Jupyter Notebook used to plot the figures below can be found at
:download:`plot_forma42.ipynb <v6.04.165/plot_forma42.ipynb>`

.. figure:: v6.04.165/forma42c_8.svg
   :height: 350 px
   :align: center

   Plot of data extracted with :red:`IMPR_RESU`

.. warning::
    
   The data extracted with :red:`RECU_FONCTION` and saved with :red:`IMPR_FONCTION` appears to be
   incorrect (possibly due to a bug).  A plot of the data is shown below.

   .. figure:: v6.04.165/forma42c_38.svg
      :height: 350 px
      :align: center

      Plot of data extracted with :red:`IMPR_FONCTION`

The reference values are identical to those for Model A, but tolerances are different.

+--------------------------------+-----------------------+---------------+
| Identification(node - element) | Reference value (kN ) | Tolerance (%) |
+================================+=======================+===============+
| N1 - M5655                     | 3519                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N6 - M5660                     | 3546                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N11 - M5664                    | 3597                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N16 - M5670                    | 3635                  | 1.00          |
+--------------------------------+-----------------------+---------------+
| N101 - M5674                   | 3614                  | 1.00          |
+--------------------------------+-----------------------+---------------+

.. _v6.04.165.5:

Summary of results
==================

This tutorial makes it possible to test three different ways of applying tension to the prestressed cables.
Each of these ways leads to different tension profiles in the cables and therefore to different stresses in
the concrete. The user should be aware of the differences.

In addition, when one associates this computation of the tension of the cable with a non-linear computation, it
is important not to accumulate losses. Thus, if the concrete is modeled using a visco-elastic model, it will not
be necessary to take into account the creep losses of the concrete in the calculation of cable tension.

It should also be noted that the tension is far from being uniform, that is the whole point of these operators
compared to imposing a prestress in the form of a prestrain or of a fictitious thermal field.

