# Square composed of one QUAD4 element and two TRI3 elements
# rotated in yz plane

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp15/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode( 0, -0.8, 0.6 )
node2 = mesh.AddNode( 0, -0.2, 1.4 )
node3 = mesh.AddNode( 0, 0, 0 )
node4 = mesh.AddNode( 0, 0.6, 0.8 )
node5 = mesh.AddNode( 0, 0.3, 0.4 )
node6 = mesh.AddNode( 0, -0.5, 1.0 )

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_1' )
node_2 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_2' )
node_3 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_3' )
node_4 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_4' )
node_5 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_5' )
node_6 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_6' )
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])

# Add tri elements
face1 = mesh.AddFace([3, 5, 6])
face2 = mesh.AddFace([3, 6, 1])

# Add quad element
face3 = mesh.AddFace([5, 4, 2, 6])

# Add groups for the faces
g_face1 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri1' )
g_face2 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri2' )
g_face3 = mesh.CreateEmptyGroup( SMESH.FACE, 'quad' )
nbAdd = g_face1.Add([1])
nbAdd = g_face2.Add([2])
nbAdd = g_face3.Add([3])

# Add segment elements
left_seg = mesh.AddEdge([1, 3])
right_seg = mesh.AddEdge([4, 2])
top_seg1 = mesh.AddEdge([2, 6])
top_seg2 = mesh.AddEdge([6, 1])
bot_seg1 = mesh.AddEdge([3, 5])
bot_seg2 = mesh.AddEdge([5, 4])

# Add groups for the segments
g_left = mesh.CreateEmptyGroup( SMESH.EDGE, 'left_edge' )
g_right = mesh.CreateEmptyGroup( SMESH.EDGE, 'right_edge' )
g_top = mesh.CreateEmptyGroup( SMESH.EDGE, 'top_edge' )
g_bot = mesh.CreateEmptyGroup( SMESH.EDGE, 'bot_edge' )
nbAdd = g_left.Add([4])
nbAdd = g_right.Add([5])
nbAdd = g_top.Add([6, 7])
nbAdd = g_bot.Add([8, 9])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp15c.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
